#!/bin/sh

. /usr/bin/pcb_common.sh

name="netdev"
datamodel="NetDev"
trace_zones=""
component="sah_services_netdev"

case $1 in
	boot|start)
		/usr/lib/netdev/netcfg.sh
		if pcb_is_running $name; then
			echo $name " already started"
		else
			pcb_app -vv -n $name -I /var/run/netdev -c /usr/lib/netdev/netdev.odl ifdef(`CONFIG_SAH_LIB_PCB_CAPABILITIES',--priv_cap_retain=CAP_NET_ADMIN)
			mtk_load nemo-core /usr/lib/nemo/modules/netdev.so
		fi
		;;
	stop)
		mtk_unload nemo-core netdev
		pcb_stop $name
		;;
	debuginfo)
		pcb_debug_info $name $component $datamodel
		;;
	log)
		action=$2
		if [ -n "$action" ]; then
			pcb_log $name $action $trace_zones
		else
			pcb_log $name enable $trace_zones
		fi
		;;
	*)
		echo "Usage : $0 [boot|start|stop|debuginfo|log]"
		;;
esac

