#!/bin/sh

[ -e /proc/sys/net/bridge/bridge-nf-call-iptables ] && \
    echo "0" > /proc/sys/net/bridge/bridge-nf-call-iptables
[ -e /proc/sys/net/bridge/bridge-nf-call-ip6tables ] && \
    echo "0" > /proc/sys/net/bridge/bridge-nf-call-ip6tables
echo "1" > /proc/sys/net/ipv4/ip_forward
echo "1" > /proc/sys/net/ipv4/conf/default/forwarding
echo "0" > /proc/sys/net/ipv4/conf/default/accept_redirects
echo "1" > /proc/sys/net/ipv4/conf/default/promote_secondaries
echo "1" > /proc/sys/net/ipv4/conf/all/forwarding
echo "0" > /proc/sys/net/ipv4/conf/all/accept_redirects
echo "1" > /proc/sys/net/ipv4/conf/all/promote_secondaries
[ -e /proc/sys/net/ipv4/netfilter/ip_conntrack_udp_timeout ] && \
    echo "120" > /proc/sys/net/ipv4/netfilter/ip_conntrack_udp_timeout
[ -e /proc/sys/net/netfilter/nf_conntrack_udp_timeout ] && \
    echo "120" > /proc/sys/net/netfilter/nf_conntrack_udp_timeout
echo "1" > /proc/sys/net/ipv6/conf/default/accept_ra
echo "1" > /proc/sys/net/ipv6/conf/default/forwarding
echo "1" > /proc/sys/net/ipv6/conf/default/disable_ipv6
echo "1" > /proc/sys/net/ipv6/conf/all/accept_ra
echo "1" > /proc/sys/net/ipv6/conf/all/forwarding
echo "1" > /proc/sys/net/ipv6/conf/all/disable_ipv6

# Respect RFC4862 5.4.5
echo "2" > /proc/sys/net/ipv6/conf/all/accept_dad
echo "2" > /proc/sys/net/ipv6/conf/default/accept_dad

