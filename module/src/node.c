/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <nemo/core.h>

#include "link.h"
#include "node.h"

#define ME "netdev"

#define CREDITS 5

typedef struct _intf_blob {
	int index;
	request_t *request;
	object_t *object;
	node_class_t *cl;
	intf_t *intf;
} link_t;

typedef enum _node_status {
	node_status_unavailable,
	node_status_ignored,
	node_status_error,
	node_status_disabled,
	node_status_binding,
	node_status_binding_dynamic,
	node_status_bound,
	node_status_dynamic,
	node_status_count
} node_status_t;

typedef struct _node {
	link_t *link;
	char *name;
	object_t *object;
	map_t *map;
	node_status_t status;
	bool busy;
	unsigned credits;
} node_t;

static const char *netdev_node_status2str(node_status_t status) {
	static const char *statusstr[node_status_count] = {
		"unavailable", "ignored", "error", "disabled", "binding", "binding", "bound", "dynamic"
	};
	if (status >= node_status_count)
		return "<unknown>";
	return statusstr[status];
}

static node_t *netdev_node_create(link_t *link, object_t *object) {
	node_t *node = calloc(1, sizeof(node_t));
	node->link = link;
	node->name = strdup(object_name(object, path_attr_key_notation));
	node->object = object;
	object_setUserData(object, node);
	return node;
}

static void netdev_node_destroy(node_t *node) {
	if (!node)
		return;
	map_destroy(node->map);
	free(node->name);
	free(node);
}

static node_t *netdev_node_find(link_t *link, const char *name) {
	object_t *object;
	object_for_each_instance(object, link->object) {
		if (!strcmp(object_name(object, path_attr_key_notation), name))
			return (node_t *)object_getUserData(object);
	}
	return NULL;
}

static void netdev_node_notify(node_t *node, node_event_t event) {
	if (node->link->cl->notify && node->object) {
		string_t path;
		string_initialize(&path, 64);
		object_path(node->object, &path, path_attr_key_notation);
		
		char *ptr0 = path.buffer, *ptr1 = NULL;
		if (ptr0 && strlen(ptr0) > 10) {
			ptr0 += 10;
			ptr1 = strchr(ptr0, '.');
		}
		if (ptr1) {
			*ptr1++ = '\0';
			SAH_TRACEZ_INFO(ME, "node-notify(intf=%s, class=%s, event=%s, spec=%s)"
				, intf_name(node->link->intf), node->link->cl->name
				, event == node_event_added ? "added" : event == node_event_modified ? "modified" : "deleted", ptr1);
			node->link->cl->notify(event, node->link->intf, ptr1, node->link->cl->userdata);
		}
		string_cleanup(&path);
	}
}

static void netdev_node_unbind(node_t *node, node_status_t target) {
	node->busy = true;

	/* Special case: if dynamic address is disabled or deleted explicitly while bound, remove it also from the system
	 * even though MAP_KILLER attribute is not set */
	if ((node->status == node_status_binding_dynamic || node->status == node_status_dynamic) && target == node_status_disabled) {
		request_t *request = request_create_deleteInstance(map_dstpath(node->map), request_common_path_key_notation);
		if (!request) {
			 SAH_TRACE_ERROR("netdev - request_create_deleteInstance(path=%s) failed", map_dstpath(node->map));
		} else {
			 if (!pcb_sendRequest(plugin_getPcb(), node->link->cl->dst, request)) {
					 SAH_TRACE_ERROR("netdev - Failed to send request: 0x%x (%s)", pcb_error, error_string(pcb_error));
			 }
			 request_destroy(request);
		}
	}
	map_destroy(node->map);
	node->map = NULL;

	node_status_t oldstatus = node->status;

	node->status = target;
	object_parameterSetCharValue(node->object, "Status", netdev_node_status2str(node->status));
	object_commit(node->object);

	if (oldstatus >= node_status_bound)
		netdev_node_notify(node, node_event_deleted);

	node->busy = false;
}

static void netdev_node_afterGetReplyHandler(map_t *map, reply_item_t *item) {
	node_t *node = map_userdata(map);
	if (reply_item_type(item) == reply_type_error) {
		netdev_node_unbind(node, node_status_error);
		return;
	}
	switch (node->status) {
	case node_status_binding:
	case node_status_binding_dynamic:
		node->status = node->status == node_status_binding ? node_status_bound : node_status_dynamic;
		object_parameterSetCharValue(node->object, "Status", netdev_node_status2str(node->status));
		object_commit(node->object);
		netdev_node_notify(node, node_event_added);
		break;
	case node_status_bound:
	case node_status_dynamic:
		if (reply_item_type(item) == reply_type_notification)
			netdev_node_notify(node, node_event_modified);
		break;
	default:
		break;
	}
}

static void netdev_node_bind(node_t *node, node_status_t target) {
	if (!node) {
		SAH_TRACEZ_ERROR(ME, "Failed to bind - node=NULL");
		return;
	}
	node->busy = true;

	if (node->link->cl->ipversion == 6 && object_parameterBoolValue(object_parent(node->link->object), "IPv6Disable")) {
		node->status = node_status_ignored;
		object_parameterSetCharValue(node->object, "Status", netdev_node_status2str(node->status));
		object_commit(node->object);

		node->busy = false;

		return;
	}
	
	node->map = map_create(node->object, target == node_status_binding_dynamic ? 0 : MAP_OWNER,
			node->link->cl->dst, "NetDev.Link.%d.%s.%s", node->link->index, node->link->cl->name, node->name);
	map_setAfterGetReplyHandler(node->map, netdev_node_afterGetReplyHandler);
	map_setUserdata(node->map, node);
	int attr = target == node_status_binding_dynamic ? MAP_LEARN : 0;
	int i;
	for (i=0; i<node->link->cl->parametercount; i++) {
		node_class_parameter_t *p = &node->link->cl->parameters[i];
		map_parameter_create(node->map, object_getParameter(node->object, p->name), NULL, attr | p->attr, NULL, NULL);
	}
	map_commit(node->map);

	node->status = target;
	object_parameterSetCharValue(node->object, "Status", netdev_node_status2str(node->status));
	object_commit(node->object);

	node->busy = false;
}

static void netdev_node_writeNode(object_t *object) {
	if (object_isTemplate(object))
		return;
	node_t *node = (node_t *)object_getUserData(object);
	if (!node) {
		link_t *link = object_getUserData(object_parent(object));
		node = netdev_node_create(link, object);
		node->credits = CREDITS;
	}
	if (node->busy)
		return;
	if (object_parameterBoolValue(object, "Enable")) {
		if (node->status <= node_status_disabled) {
			netdev_node_bind(node, node_status_binding);
		} else {
			map_commit(node->map);
		}
	} else {
		node->credits = CREDITS;
		if (node->status > node_status_disabled) {
			netdev_node_unbind(node, node_status_disabled);
		} else if (node->status <= node_status_error) {
			node->status = node_status_disabled;
			object_parameterSetCharValue(node->object, "Status", "disabled");
			object_commit(node->object);
		}
	}	
}

static bool netdev_node_deleteNode(object_t *object, object_t *instance) {
	(void)object;
	node_t *node = object_getUserData(instance);
	if (!node || node->busy)
		return true;
	if (node->status > node_status_disabled) {
		netdev_node_unbind(node, node_status_disabled);
		netdev_node_destroy(node);
	}
	return true;
}

static bool netdev_node_replyHandler(request_t *request, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)pcb; (void)from; 
	link_t *link = (link_t *)userdata;
	reply_t *reply = request_reply(request);
	if (!reply)
		return true;
	reply_item_t *item;
	for(item = reply_firstItem(reply); item; item = reply_nextItem(item)) {
		if (reply_item_type(item) == reply_type_notification) {
			notification_t *notification = reply_item_notification(item);
			if (notification_type(notification) != notify_object_deleted)
				continue;
			const char *name = strrchr(notification_objectPath(notification), '.');
			if (!name)
				continue;
			++name;
			node_t *node = netdev_node_find(link, name);
			if (!node) {
				SAH_TRACEZ_INFO(ME, "NeMo counterpart of NetDev.Link.%d.%s.%s not found", link->index, link->cl->name, name);
			} else {
				switch (node->status) {
				case node_status_binding_dynamic:
				case node_status_dynamic:
					SAH_TRACEZ_INFO(ME, "Delete node NetDev.Link.%d.%s.%s", link->index, link->cl->name, name);
					netdev_node_unbind(node, node_status_error);
					object_delete(node->object);
					object_commit(node->object);
					netdev_node_destroy(node);
					break;
				case node_status_binding:
				case node_status_bound:
					SAH_TRACEZ_INFO(ME, "Unbind node NetDev.Link.%d.%s.%s", link->index, link->cl->name, name);
					netdev_node_unbind(node, node_status_error);
					if (node->credits) {
						netdev_node_bind(node, node_status_binding);
						node->credits--;
					}
					break;
				default:
					break;
				}
			}
		} else if (reply_item_type(item) == reply_type_object) {
			object_t *object = reply_item_object(item);
			if (!object_isInstance(object))
				continue;
			const char *name = object_name(object, path_attr_key_notation);
			if (!name)
				continue;
			node_t *node = netdev_node_find(link, name);
			if (node) {
				SAH_TRACEZ_INFO(ME, "name collision detected, ignoring NetDev.Link.%d.%s.%s", link->index, link->cl->name, name);
			} else {
				SAH_TRACEZ_INFO(ME, "Create dynamic node NetDev.Link.%d.%s.%s", link->index, link->cl->name, name);
				object_t *obj = object_createInstance(link->object, 0, name);
				if(!obj) {
					SAH_TRACEZ_NOTICE(ME, "Failed to create dynamic node - %s", error_string(pcb_error));
					continue;
				}
				object_setPersistent(obj, false);
				node = netdev_node_create(link, obj);
				object_parameterSetBoolValue(obj, "Enable", true);
				netdev_node_bind(node, node_status_binding_dynamic);
			}
		}
	}
	return true;
}


static void netdev_node_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata; (void)flag;
	node_class_t *cl = (node_class_t *)userdata;
	if (value)
		value = flagset_check(intf_flagset(intf), "netdev-bound") && flagset_check(intf_flagset(intf), cl->flag);
	if (value) {
		link_t *link = calloc(1, sizeof(link_t));
		link->object = object_getObject(plugin_intf2object(intf), cl->name, 0, NULL);
		link->cl = cl;
		link->index = netdev_link_intf2index(intf);
		link->intf = intf;
		intf_addBlob(intf, cl, link);
		object_setUserData(link->object, link);
		object_setWriteHandler(link->object, netdev_node_writeNode);
		object_setInstanceDelHandler(link->object, netdev_node_deleteNode);
		object_t *object;
		object_for_each_instance(object, link->object) {
			object_setWriteHandler(object, netdev_node_writeNode);
			object_setInstanceDelHandler(object, netdev_node_deleteNode);
			object_parameterSetCharValue(object, "Status", "disabled");
			object_commit(object); // so we will get in write handler for the first time
		}
		char path[64];
		sprintf(path, "NetDev.Link.%d.%s", link->index, cl->name);
		link->request = request_create_getObject(path, 1, request_no_object_caching | request_common_path_key_notation | request_notify_object_added | request_notify_object_deleted);
		request_setData(link->request, link);
		request_setReplyHandler(link->request, netdev_node_replyHandler);
		if (!pcb_sendRequest(plugin_getPcb(), cl->dst, link->request))
			SAH_TRACE_ERROR("netdev - Failed to send request");
	} else {
		link_t *link = intf_getBlob(intf, cl);
		if (!link)
			return;
		object_t *object, *nextobject;
		object_setUserData(link->object, NULL);
		object_setWriteHandler(link->object, NULL);
		object_setInstanceDelHandler(link->object, NULL);
		for (object = object_firstInstance(link->object); object; object = nextobject) {
			nextobject = object_nextInstance(object);
			node_t *node = (node_t *)object_getUserData(object);
			if (!node)
				continue;
			object_setWriteHandler(object, NULL);
			object_setInstanceDelHandler(object, NULL);
			object_setUserData(object, NULL);
			switch (node->status) {
			case node_status_binding:
			case node_status_bound:
				netdev_node_unbind(node, node_status_unavailable);
				object_parameterSetCharValue(object, "Status", "unavailable");
				break;
			case node_status_binding_dynamic:
			case node_status_dynamic:
				netdev_node_unbind(node, node_status_unavailable);
				object_delete(object);
				break;
			default:
				break;
			}
			object_commit(object);
			netdev_node_destroy(node);
		}
		request_destroy(link->request);
		intf_delBlob(intf, cl);
		free(link);
	}	
}

void netdev_node_class_initialize(node_class_t *cl) {
	intf_addFlagListener(NULL, "netdev-bound", netdev_node_rootHandler, cl, true);
	intf_addFlagListener(NULL, cl->flag, netdev_node_rootHandler, cl, true);
}

void netdev_node_class_cleanup(node_class_t *cl) {
	intf_delFlagListener(NULL, "netdev-bound", netdev_node_rootHandler, cl, true);
	intf_delFlagListener(NULL, cl->flag, netdev_node_rootHandler, cl, true);
}

void netdev_node_class_recover(node_class_t *cl, intf_t *intf) {
	link_t *link = intf_getBlob(intf, cl);
	if (!link)
		return;
	object_t *object;
	object_for_each_instance(object, link->object) {
		node_t *node = (node_t *)object_getUserData(object);
		if (!node)
			continue;
		switch (node->status) {
		case node_status_error:
		case node_status_ignored:
			netdev_node_bind(node, node_status_binding);
			break;
		case node_status_binding:
		case node_status_bound:
			break;
		default:
			break;
		}
	}
}

