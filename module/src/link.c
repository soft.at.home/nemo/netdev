/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <nemo/core.h>

typedef enum _netdev_status {
	netdev_status_unavailable,
	netdev_status_bound,
} netdev_status_t;

typedef enum _netdev_bind_type {
	netdev_bind_type_index,
	netdev_bind_type_name,
} netdev_bind_type_t;

typedef struct _pintf pintf_t;

typedef struct _intf_blob {
	netdev_status_t status;
	netdev_bind_type_t bind_type;
	intf_t *intf;
	int index;
	pintf_t *pintf;
	map_t *map;
	bool busy;
} nintf_t;

struct _pintf {
	llist_iterator_t it;
	int index;
	char *name;
	nintf_t *nintf;
};

static int dummy;
static void *blobkey = &dummy;

static llist_t pintfs = {NULL, NULL};
static request_t *listener = NULL;

static peer_info_t *dst = NULL;

static pintf_t *netdev_pintf_create(int index) {
	pintf_t *pintf = calloc(1, sizeof(pintf_t));
	pintf->index = index;
	llist_append(&pintfs, &pintf->it);
	return pintf;
}

static pintf_t *netdev_pintf_find(int index) {
	pintf_t *pintf;
	for (pintf = (pintf_t *)llist_first(&pintfs); pintf; pintf = (pintf_t *)llist_iterator_next(&pintf->it)) {
		if (pintf->index == index)
			return pintf;
	}
	return NULL;
}

static void netdev_nintf_setStatus(nintf_t *nintf, netdev_status_t status) {
	if (nintf->status == status)
		return;
	if (nintf->status == netdev_status_bound)
		flagset_clear(intf_flagset(nintf->intf), "netdev-bound");
	nintf->status = status;
}

static bool netdev_link_match(nintf_t *nintf, pintf_t *pintf) {
	if (nintf->bind_type == netdev_bind_type_index) {
		return nintf->index == pintf->index;
	} else {
		object_t *object = plugin_intf2object(nintf->intf);
		const char *netdevname = object_da_parameterCharValue(object, "NetDevName");
		bool name_match = false;

		if (netdevname && *netdevname) {
			name_match = !strcmp(netdevname, pintf->name?pintf->name:"");
		} else {
			name_match = !strcmp(intf_name(nintf->intf), pintf->name?pintf->name:"");
		}

		if (nintf->index) { // allow to explicitly unbind if bound based on name by modifying NetDevIndex
			return name_match && nintf->index == pintf->index;
		} else {
			return name_match;
		}
	}
}

static void netdev_link_afterGetReplyHandler(map_t *map, reply_item_t *item) {
	nintf_t *nintf = map_userdata(map);
	flagset_set(intf_flagset(nintf->intf), "netdev-bound");

	const char *netdevflags = NULL;
	if (reply_item_type(item) == reply_type_object) {
		object_t *object = reply_item_object(item);
		netdevflags = object_da_parameterCharValue(object, "Flags");
	} else if (reply_item_type(item) == reply_type_notification) {
		notification_t *notification = reply_item_notification(item);
		if (notification_type(notification) != notify_value_changed)
			return;
		const char *dstname = variant_da_char(notification_parameter_variant(notification_getParameter(notification, "parameter")));
		if (!strcmp(dstname?:"", "Flags"))
			netdevflags = variant_da_char(notification_parameter_variant(notification_getParameter(notification, "newvalue")));
	}
	if (!netdevflags)
		return;
	bool up = strstr(netdevflags, "up") ? true : false;
	if (up)
		flagset_set(intf_flagset(nintf->intf), "netdev-up");
	else
		flagset_clear(intf_flagset(nintf->intf), "netdev-up");
}

static void netdev_link_bind(nintf_t *nintf, pintf_t *pintf) {
	nintf->busy = true;
	object_t *object = plugin_intf2object(nintf->intf);
	parameter_t *parameter;

	if (!nintf->index) {
		object_parameterSetInt32Value(object, "NetDevIndex", pintf->index);
		object_commit(object);
		nintf->index = pintf->index;
	}

	nintf->map = map_create(object, 0, dst, "NetDev.Link.%d", pintf->index);

	map_setAfterGetReplyHandler(nintf->map, netdev_link_afterGetReplyHandler);
	map_setUserdata(nintf->map, nintf);

	const char *charvalue;
	unsigned long uint32value;

	map_parameter_create(nintf->map, object_getParameter(object, "NetDevType"), "Type", MAP_READONLY, NULL, NULL);

	parameter = object_getParameter(object, "NetDevFlags");
	charvalue = string_buffer(variant_da_string(parameter_getValue(parameter)));
	map_parameter_create(nintf->map, parameter, "Flags", charvalue && *charvalue ? MAP_WRITEONLY : MAP_LEARN, NULL, NULL);

	parameter = object_getParameter(object, "NetDevName");
	charvalue = string_buffer(variant_da_string(parameter_getValue(parameter)));
	map_parameter_create(nintf->map, parameter, "Name", charvalue && *charvalue ? 0 : MAP_LEARN, NULL, NULL);

	parameter = object_getParameter(object, "LLAddress");
	charvalue = string_buffer(variant_da_string(parameter_getValue(parameter)));
	map_parameter_create(nintf->map, parameter, NULL, charvalue && *charvalue ? 0 : MAP_LEARN, NULL, NULL);

	parameter = object_getParameter(object, "TxQueueLen");
	uint32value = variant_uint32(parameter_getValue(parameter));
	map_parameter_create(nintf->map, parameter, NULL, uint32value ? 0 : MAP_LEARN, NULL, NULL);

	parameter = object_getParameter(object, "MTU");
	uint32value = variant_uint32(parameter_getValue(parameter));
	map_parameter_create(nintf->map, parameter, NULL, uint32value ? 0 : MAP_LEARN, NULL, NULL);

	map_parameter_create(nintf->map, object_getParameter(object, "NetDevState"), "State", MAP_READONLY, NULL, NULL);

	map_parameter_create(nintf->map, object_getParameter(object, "IPv4Forwarding"), "IPv4Forwarding", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv4ForceIGMPVersion"), "IPv4ForceIGMPVersion", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv4AcceptSourceRoute"), "IPv4AcceptSourceRoute", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv4AcceptRedirects"), "IPv4AcceptRedirects", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv4ArpFilter"), "IPv4ArpFilter", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv4AcceptLocal"), "IPv4AcceptLocal", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv6AcceptRA"), "IPv6AcceptRA", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv6ActAsRouter"), "IPv6ActAsRouter", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv6AutoConf"), "IPv6AutoConf", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv6MaxRtrSolicitations"), "IPv6MaxRtrSolicitations", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv6RtrSolicitationInterval"), "IPv6RtrSolicitationInterval", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv6AcceptSourceRoute"), "IPv6AcceptSourceRoute", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv6AcceptRedirects"), "IPv6AcceptRedirects", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv6OptimisticDAD"), "IPv6OptimisticDAD", MAP_WRITEONLY, NULL, NULL);
	map_parameter_create(nintf->map, object_getParameter(object, "IPv6Disable"), "IPv6Disable", MAP_WRITEONLY, NULL, NULL);

	map_commit(nintf->map);

	nintf->pintf = pintf;
	pintf->nintf = nintf;
	netdev_nintf_setStatus(nintf, netdev_status_bound);

	nintf->busy = false;
}

static void netdev_link_unbind(nintf_t *nintf, pintf_t *pintf) {
	nintf->busy = true;

	nintf->pintf = NULL;
	pintf->nintf = NULL;

	object_t *object = plugin_intf2object(nintf->intf);

	if (map_parameter_attributes(map_parameter_findByName(nintf->map, "NetDevFlags")) & MAP_LEARN)
		object_parameterSetCharValue(object, "NetDevFlags", "");
	if (map_parameter_attributes(map_parameter_findByName(nintf->map, "NetDevName")) & MAP_LEARN)
		object_parameterSetCharValue(object, "NetDevName", "");
	if (map_parameter_attributes(map_parameter_findByName(nintf->map, "LLAddress")) & MAP_LEARN)
		object_parameterSetCharValue(object, "LLAddress", "");
	if (map_parameter_attributes(map_parameter_findByName(nintf->map, "TxQueueLen")) & MAP_LEARN)
		object_parameterSetUInt32Value(object, "TxQueueLen", 0);
	if (map_parameter_attributes(map_parameter_findByName(nintf->map, "MTU")) & MAP_LEARN)
		object_parameterSetUInt32Value(object, "MTU", 0);
	object_parameterSetCharValue(object, "NetDevState", "unknown");

	map_destroy(nintf->map);
	nintf->map = NULL;

	netdev_nintf_setStatus(nintf, netdev_status_unavailable);

	flagset_clear(intf_flagset(nintf->intf), "netdev-up");

	if (nintf->bind_type == netdev_bind_type_name && nintf->index == pintf->index) {
		object_parameterSetInt32Value(object, "NetDevIndex", 0);
		nintf->index = 0;
	}
	object_commit(object);

	nintf->busy = false;
}

static void netdev_pintf_destroy(pintf_t *pintf) {
	if (pintf->nintf)
		netdev_link_unbind(pintf->nintf, pintf);
	llist_iterator_take(&pintf->it);
	free(pintf->name);
	free(pintf);
}

static void netdev_nintf_unbind(nintf_t *nintf) {
	if (nintf->status != netdev_status_bound)
		return;
	netdev_link_unbind(nintf, nintf->pintf);
}

static void netdev_pintf_unbind(pintf_t *pintf) {
	if (!pintf->nintf)
		return;
	netdev_link_unbind(pintf->nintf, pintf);
}

static void netdev_nintf_bind(nintf_t *nintf) {
	if (nintf->status != netdev_status_unavailable)
		return;
	if (nintf->index)
		nintf->bind_type = netdev_bind_type_index;
	else
		nintf->bind_type = netdev_bind_type_name;
	pintf_t *pintf;
	for (pintf = (pintf_t *)llist_first(&pintfs); pintf; pintf = (pintf_t *)llist_iterator_next(&pintf->it)) {
		if (netdev_link_match(nintf, pintf)) {
			netdev_pintf_unbind(pintf); // if the link is bound to another netdev, steal the binding
			netdev_link_bind(nintf, pintf);
			break;
		}
	}
}

static void netdev_pintf_bind(pintf_t *pintf) {
	if (pintf->nintf)
		return;
	intf_t *intf;
	nintf_t *nintf;
	for (intf = intf_first(); intf; intf = intf_next(intf)) {
		nintf = intf_getBlob(intf, blobkey);
		if (!nintf)
			continue;
		if (netdev_link_match(nintf, pintf)) {
			netdev_nintf_unbind(nintf); // if the netdev is bound to another link, steal the binding
			netdev_link_bind(nintf, pintf);
			break;
		}
	}
}

static bool netdev_nintf_check(nintf_t *nintf) {
	if (nintf->status != netdev_status_bound)
		return true;
	return netdev_link_match(nintf, nintf->pintf);
}

static bool netdev_pintf_check(pintf_t *pintf) {
	if (!pintf->nintf)
		return true;
	return netdev_link_match(pintf->nintf, pintf);
}

static void netdev_link_setNetDevState(intf_t *intf, const char *value) {
	if (value && !strcmp(value, "up"))
		flagset_set(intf_flagset(intf), "up");
	else
		flagset_clear(intf_flagset(intf), "up");
}

static void netdev_link_writeNetDevState(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	netdev_link_setNetDevState(plugin_object2intf(parameter_owner(parameter)),
	                           string_buffer(variant_da_string(parameter_getValue(parameter))));
}

static void netdev_link_intfFlagHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)intf;
	nintf_t *nintf = (nintf_t *)userdata;
	if (nintf->busy)
		return;
	if (!strcmp(flag, "netdev-monitor")) {
		parameter_t *netdevstate = object_getParameter(plugin_intf2object(intf), "NetDevState");
		if (value) {
			plugin_setParameterWriteHandler(netdevstate, netdev_link_writeNetDevState);
			netdev_link_setNetDevState(intf, string_buffer(variant_da_string(parameter_getValue(netdevstate))));
		} else {
			netdev_link_setNetDevState(intf, NULL);
			plugin_setParameterWriteHandler(netdevstate, NULL);
		}
	}
}

static void netdev_link_intfMgmtHandler(intf_t *intf, void *userdata) {
	nintf_t *nintf = (nintf_t *)userdata;
	if (nintf->busy)
		return;
	nintf->index = (int)object_parameterInt32Value(plugin_intf2object(intf), "NetDevIndex");

	if (!netdev_nintf_check(nintf))
		netdev_nintf_unbind(nintf);
	if (nintf->status == netdev_status_bound)
		map_commit(nintf->map);
	else if (nintf->status == netdev_status_unavailable)
		netdev_nintf_bind(nintf);
}

static function_exec_state_t netdev_link_setNetDevFlag(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	variant_initialize(retval, variant_type_unknown);
	uint32_t attr = request_attributes(fcall_request(fcall));
	object_t *obj = fcall_object(fcall);
	intf_t *intf = plugin_object2intf(obj);
	bool res = false;
	char *union_flagset = NULL;

	/* parse current NetDevFlags */
	flagset_t *flagset1 = flagset_create(false);
	const char *oldset = object_da_parameterCharValue(obj, "NetDevFlags");
	SAH_TRACE_INFO("netdev - [%s]setNetDevFlag: old set[%s]", intf_name(intf), oldset);
	flagset_parse(flagset1, oldset);

	/* parse the new NetDevFlags */
	char *flags = NULL;
	flagset_t *flagset2 = flagset_create(false);
	argument_getChar(&flags, args, attr, "flags", NULL);
	SAH_TRACE_INFO("netdev - [%s]setNetDevFlag: new set[%s]", intf_name(intf), flags);
	res = flagset_parse(flagset2, flags);
	if (!res) {
		SAH_TRACE_ERROR("netdev - [%s]setNetDevFlag: parse error[%s]", intf_name(intf), flags);
		reply_addError(request_reply(fcall_request(fcall)), pcb_error_invalid_parameter, "Invalid flags", "flags");
		goto out;
	}

	/* store a union of both old set and new flag set */
	flagset_union(flagset1, flagset2);
	union_flagset = flagset_print(flagset1);
	SAH_TRACE_NOTICE("netdev - [%s]union: set[%s]", intf_name(intf), union_flagset);
	object_parameterSetCharValue(obj, "NetDevFlags", union_flagset);
	object_commit(obj);

out:
	flagset_destroy(flagset1); flagset1 = NULL;
	flagset_destroy(flagset2); flagset2 = NULL;

	free(union_flagset); union_flagset = NULL;
	free(flags); flags = NULL;
	fcall_destroy(fcall);
	return function_exec_done;
}

static function_exec_state_t netdev_link_clearNetDevFlag(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	variant_initialize(retval, variant_type_unknown);
	uint32_t attr = request_attributes(fcall_request(fcall));
	object_t *obj = fcall_object(fcall);
	intf_t *intf = plugin_object2intf(obj);
	bool res = false;
	char *subtract_flagset = NULL;

	/* parse current NetDevFlags */
	flagset_t *flagset1 = flagset_create(false);
	const char *oldset = object_da_parameterCharValue(obj, "NetDevFlags");
	SAH_TRACE_INFO("netdev - [%s]clearNetDevFlag: old set[%s]", intf_name(intf), oldset);
	flagset_parse(flagset1, oldset);

	/* parse the new NetDevFlags */
	char *flags = NULL;
	flagset_t *flagset2 = flagset_create(false);
	argument_getChar(&flags, args, attr, "flags", NULL);
	SAH_TRACE_INFO("netdev - [%s]clearNetDevFlag: new set[%s]", intf_name(intf), flags);
	res = flagset_parse(flagset2, flags);
	if (!res) {
		SAH_TRACE_ERROR("netdev - [%s]clearNetDevFlag: parse error[%s]", intf_name(intf), flags);
		reply_addError(request_reply(fcall_request(fcall)), pcb_error_invalid_parameter, "Invalid flags", "flags");
		goto out;
	}

	/* store the subtract of both old set and new flag set */
	flagset_subtract(flagset1, flagset2);
	subtract_flagset = flagset_print(flagset1);
	SAH_TRACE_NOTICE("netdev - [%s]subtract: set[%s]", intf_name(intf), subtract_flagset);
	object_parameterSetCharValue(obj, "NetDevFlags", subtract_flagset);
	object_commit(obj);

out:
	flagset_destroy(flagset1); flagset1 = NULL;
	flagset_destroy(flagset2); flagset2 = NULL;

	free(subtract_flagset); subtract_flagset = NULL;
	free(flags); flags = NULL;
	fcall_destroy(fcall);
	return function_exec_done;
}

static function_exec_state_t netdev_link_refreshNetDev(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)args; (void)retval;
	intf_t *intf = plugin_object2intf(fcall_object(fcall));
	nintf_t *nintf = (nintf_t *)intf_getBlob(intf, blobkey);
	if (nintf->map)
		map_refresh(nintf->map);
	fcall_destroy(fcall);
	return function_exec_done;
}

static void netdev_link_rootIntfFlagHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata; (void)flag;
	nintf_t *nintf;
	object_t *object = plugin_intf2object(intf);
	if (value) {
		nintf = calloc(1, sizeof(nintf_t));
		nintf->intf = intf;
		nintf->index = (int)object_parameterInt32Value(object, "NetDevIndex");
		intf_addBlob(intf, blobkey, nintf);
		netdev_nintf_bind(nintf);
		intf_addFlagListener(intf, NULL, netdev_link_intfFlagHandler, nintf, true);
		intf_addMgmtListener(intf, NULL, netdev_link_intfMgmtHandler, NULL, nintf, false);
		function_setHandler(object_getFunction(object, "refreshNetDev"), netdev_link_refreshNetDev);
		function_setHandler(object_getFunction(object, "setNetDevFlag"), netdev_link_setNetDevFlag);
		function_setHandler(object_getFunction(object, "clearNetDevFlag"), netdev_link_clearNetDevFlag);
		const char *netdevname = object_da_parameterCharValue(object, "NetDevName");
		if (netdevname && *netdevname) {
			int idx = if_nametoindex(netdevname);
			if (idx > 0) {
				SAH_TRACE_WARNING("netdev - set index[%s|%d]", netdevname, idx);
				object_parameterSetInt32Value(object, "NetDevIndex", idx);
				object_commit(object);
			}
		}
	} else {
		function_setHandler(object_getFunction(object, "refreshNetDev"), NULL);
		function_setHandler(object_getFunction(object, "setNetDevFlag"), NULL);
		function_setHandler(object_getFunction(object, "clearNetDevFlag"), NULL);
		nintf = intf_getBlob(intf, blobkey);
		if (!nintf) {
			SAH_TRACE_ERROR("netdev - intf %s has no netdev blob", intf_name(intf));
			return;
		}
		netdev_nintf_unbind(nintf);
		intf_delFlagListener(intf, NULL, netdev_link_intfFlagHandler, nintf, true);
		intf_delMgmtListener(intf, NULL, netdev_link_intfMgmtHandler, NULL, nintf, false);
		intf_delBlob(intf, blobkey);
		free(nintf);
	}
}

static bool netdev_link_rootReplyHandler(request_t *request, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)pcb; (void)from; (void)userdata;
	reply_t *reply = request_reply(request);
	if (!reply)
		return true;
	reply_item_t *item;
	for(item = reply_firstItem(reply); item; item = reply_nextItem(item)) {
		switch(reply_item_type(item)) {
			case reply_type_object: {
				object_t *object = reply_item_object(item);
				int index = (int)strtol(object_name(object, 0), NULL, 0);
				if (!index)
					break;
				pintf_t *pintf = netdev_pintf_find(index);
				if (!pintf)
					pintf = netdev_pintf_create(index);
				free(pintf->name);
				pintf->name = object_parameterCharValue(object, "Name");
				if (!netdev_pintf_check(pintf))
					netdev_pintf_unbind(pintf);
				netdev_pintf_bind(pintf);
				break;
			}
			case reply_type_notification: {
				notification_t *notification = reply_item_notification(item);
				if (notification_type(notification) != notify_object_deleted)
					break;
				char *name = strrchr(notification_objectPath(notification), '.');
				if (!name)
					break;
				int index = (int)strtol(++name, NULL, 0);
				pintf_t *pintf = netdev_pintf_find(index);
				if (!pintf)
					break;
				nintf_t *nintf = pintf->nintf;
				netdev_pintf_unbind(pintf);
				netdev_pintf_destroy(pintf);
				/* Because links are deleted with mercy in the NetDev data model, it may happen that if an interface is deleted
				 * and then a new one is created with the same name immediately thereafter, we get the new event first
				 * and then the delete event, so check after deleting a link if there is already a new one waiting to get bound
				 * with this NeMo Intf.
				 */
				if (nintf)
					netdev_nintf_bind(nintf);
				break;
			}
			default: break;
		}
	}
	return true;
}

void netdev_link_initialize(peer_info_t *dstref) {
	dst = dstref;
	intf_addFlagListener(NULL, "netdev", netdev_link_rootIntfFlagHandler, NULL, true);
	listener = request_create_getObject("NetDev.Link", 1,
			request_no_object_caching | request_getObject_parameters | request_notify_values_changed | request_notify_object_added | request_notify_object_deleted);
	request_addParameter(listener, "Name", NULL);
	request_setReplyHandler(listener, netdev_link_rootReplyHandler);
	if (!pcb_sendRequest(plugin_getPcb(), dst, listener))
		SAH_TRACE_ERROR("netdev - Failed to send request");
}

void netdev_link_cleanup() {
	intf_delFlagListener(NULL, "netdev", netdev_link_rootIntfFlagHandler, NULL, true);
	request_destroy(listener);
	pintf_t *link;
	while (!llist_isEmpty(&pintfs)) {
		link = (pintf_t *)llist_first(&pintfs);
		netdev_pintf_unbind(link);
		netdev_pintf_destroy(link);
	}
}

int netdev_link_intf2index(intf_t *intf) {
	nintf_t *netdev = intf_getBlob(intf, blobkey);
	if (!netdev) {
		SAH_TRACE_WARNING("netdev - netdev blob not found");
		return 0;
	}
	return netdev->index;
}

