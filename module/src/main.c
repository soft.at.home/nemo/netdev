/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <mtk.h>

#include <nemo/core.h>

#include "link.h"
#include "node.h"
#include "api.h"

void netdev_stats_initialize();
void netdev_stats_cleanup();

void gre_initialize();
void gre_cleanup();

static node_class_parameter_t addr_parameters[] = {
	{ "Address", 0 },
	{ "Peer", 0 },
	{ "PrefixLen", 0 },
	{ "Flags", 0 },
	{ "Scope", 0 },
	{ "NeMoFlags", 0 },
	{ "PreferredLifetime", 0 },
	{ "ValidLifetime", 0 },
//(*)	{ "CreatedTimestamp", MAP_READONLY },
//(*)	{ "UpdatedTimestamp", MAP_READONLY },
};

static node_class_parameter_t route_parameters[] = {
	{ "DstLen", 0 },
	{ "Table", 0 },
	{ "Protocol", 0 },
	{ "Scope", 0 },
	{ "Type", 0 },
	{ "Dst", 0 },
	{ "Priority", 0 },
	{ "Gateway", 0 },
//(*)	{ "PrefSrc", 0 },
//(*)	{ "MTU", 0 },
//(*)	{ "AdvMSS", 0 },
//(*)	{ "HopLimit", 0 },
};

//(*)static node_class_parameter_t neigh_parameters[] = {
//(*)	{ "Family", 0 },
//(*)	{ "Flags", 0 },
//(*)	{ "Dst", 0 },
//(*)	{ "LLAddress", 0 },
//(*)	{ "State", 0 },
//(*)};

static void v4addr_notify(node_event_t event, intf_t *intf, const char *spec, void *userdata);
static void v6addr_notify(node_event_t event, intf_t *intf, const char *spec, void *userdata);

static node_class_t ipv4addr_class = { NULL, "IPv4Addr", addr_parameters, /* (*)9 */6, v4addr_notify, NULL, "ipv4", 4 };
static node_class_t ipv6addr_class = { NULL, "IPv6Addr", addr_parameters, /* (*)9 */8, v6addr_notify, NULL, "ipv6", 6 };
static node_class_t ipv4route_class = { NULL, "IPv4Route", route_parameters, /* (*) 12 */8, NULL, NULL, "ipv4", 4 };
static node_class_t ipv6route_class = { NULL, "IPv6Route", route_parameters, /* (*) 12 */8, NULL, NULL, "ipv6", 6 };
//(*)static node_class_t neigh_class = { NULL, "Neigh", neigh_parameters, 5, NULL, NULL, 0 };

static void netdev_check_ipvx_up(intf_t *intf, const char *name, const char *flag) {
	object_t *object = object_getObject(plugin_intf2object(intf), name, 0, NULL);
	bool up = false;
	object_for_each_instance(object, object) {
		const char *status = object_da_parameterCharValue(object, "Status");
		if (!status)
			continue;
		if (strcmp(status, "bound") && strcmp(status, "dynamic"))
			continue;
		const char *flags = object_da_parameterCharValue(object, "Flags");
		if (!flags)
			continue;
		if (strstr(flags, "tentative") || strstr(flags, "deprecated"))
			continue;
		up = true;
	}
	if (up) {
		flagset_set(intf_flagset(intf), flag);
	}
	else {
		flagset_clear(intf_flagset(intf), flag);
	}
}

static void v4addr_notify(node_event_t event, intf_t *intf, const char *spec, void *userdata) {
	(void)spec; (void)userdata;
	netdev_check_ipvx_up(intf, "IPv4Addr", "ipv4-up");
	netdev_api_query_invalidate();
	if (event == node_event_added || event == node_event_modified)
		netdev_node_class_recover(&ipv4route_class, intf);
}

static void v6addr_notify(node_event_t event, intf_t *intf, const char *spec, void *userdata) {
	(void)spec; (void)userdata;
	netdev_check_ipvx_up(intf, "IPv6Addr", "ipv6-up");
	netdev_api_query_invalidate();
	if (event == node_event_added || event == node_event_modified)
		netdev_node_class_recover(&ipv6route_class, intf);
}

static void netdev_intfMibHandler(intf_t *intf, intf_mibevent_t event, const char *path, void *userdata) {
	(void)userdata;

	if (event != intf_mibevent_updated || strcmp(path, "IPv6Disable")) {
		return;
	}

	if (!object_parameterBoolValue(plugin_intf2object(intf), path)) {
		netdev_node_class_recover(&ipv6addr_class, intf);
		netdev_node_class_recover(&ipv6route_class, intf);
	}
}

static void netdev_intfFlagHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;
	if (!value)
		return;
	if (!strcmp(flag, "netdev-up")) {
		netdev_node_class_recover(&ipv6addr_class, intf);
		netdev_node_class_recover(&ipv4route_class, intf);
		netdev_node_class_recover(&ipv6route_class, intf);
	} else if (!strcmp(flag, "ipv6-up")) {
		netdev_node_class_recover(&ipv6addr_class, intf);
	}
}

static peer_info_t *netdev_dst = NULL;

static bool netdev_start(argument_value_list_t *args) {
	char *dsturi = NULL;
	const char *cdsturi;
	arg_takeChar(&dsturi, args, ARG_BYNAME, "dst");
	cdsturi = dsturi && *dsturi ? dsturi : "pcb://ipc:[/var/run/netdev]";
	uri_t *uri = NULL;
	netdev_dst = connection_connect(pcb_connection(plugin_getPcb()), cdsturi, &uri);
	if (!netdev_dst) {
		SAH_TRACE_ERROR("netdev - Connection to %s failed - fallback to sysbus", cdsturi);
		// no fatal error, we still have fallback over the sysbus.
	}
	if (uri)
		uri_destroy(uri);
	if (!netdev_dst)
		netdev_dst = plugin_getPeer();
	free(dsturi);
	netdev_api_initialize();
	netdev_link_initialize(netdev_dst);
	ipv4addr_class.dst = ipv6addr_class.dst = ipv4route_class.dst = ipv6route_class.dst = /* (*) neigh_class.dst = */ netdev_dst;
	netdev_node_class_initialize(&ipv4addr_class);
	netdev_node_class_initialize(&ipv6addr_class);
	netdev_node_class_initialize(&ipv4route_class);
	netdev_node_class_initialize(&ipv6route_class);
	intf_addFlagListener(NULL, NULL, netdev_intfFlagHandler, NULL, false);
	intf_addMibListener(NULL, netdev_intfMibHandler, NULL);
	netdev_stats_initialize(netdev_dst);
//(*)	node_class_initialize(&neigh_class);
	gre_initialize(netdev_dst);
	return true;
}

static void netdev_stop() {
	gre_cleanup();
	netdev_stats_cleanup();
	intf_delMibListener(NULL, netdev_intfMibHandler, NULL);
	intf_delFlagListener(NULL, NULL, netdev_intfFlagHandler, NULL, false);
	netdev_node_class_cleanup(&ipv4addr_class);
	netdev_node_class_cleanup(&ipv6addr_class);
	netdev_node_class_cleanup(&ipv4route_class);
	netdev_node_class_cleanup(&ipv6route_class);
//(*)	node_class_cleanup(&neigh_class);
	netdev_link_cleanup();
	netdev_api_cleanup();
	if (netdev_dst != plugin_getPeer())
		peer_destroy(netdev_dst);
}

static mtk_module_info_t netdev_info = {
	.name  = "netdev",
	.start = netdev_start,
	.stop  = netdev_stop
};

__attribute__((constructor)) static void netdev_init() {
	mtk_module_register(&netdev_info);
}

//(*) descope for performance / memory optimization
