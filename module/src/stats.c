/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <nemo/core.h>

#include "link.h"

#define ME "netdev"

typedef struct _call {
	llist_iterator_t it;
	intf_t *intf;
	function_call_t *fcall;
	request_t *request;
	variant_t retval;
	unsigned long state;
} call_t;

static llist_t calls = {NULL, NULL};

static peer_info_t *dst = NULL;

static void netdev_stats_call_destroy(call_t *call) {
	if (!call)
		return;
	SAH_TRACEZ_INFO(ME, "netdev_stats_call_destroy(%s)", intf_name(call->intf));
	if (call->request)
		request_destroy(call->request);
	if (call->fcall) {
		fcall_setCancelHandler(call->fcall, NULL);
		fcall_destroy(call->fcall);
	}
	variant_cleanup(&call->retval);
	llist_iterator_take(&call->it);
	free(call);
}

static void netdev_stats_cancelHandler(function_call_t *fcall, void *userdata) {
	call_t *call = (call_t *)userdata;
	fcall_setCancelHandler(fcall, NULL);
	call->fcall = NULL;
	netdev_stats_call_destroy(call);
}

static call_t *netdev_stats_call_create(intf_t *intf, function_call_t *fcall) {
	SAH_TRACEZ_INFO(ME, "netdev_stats_call_create(%s)", intf_name(intf));
	call_t *call = calloc(1, sizeof(call_t));
	llist_append(&calls, &call->it);
	call->intf = intf;
	call->fcall = fcall;
	fcall_setUserData(fcall, call);
	fcall_setCancelHandler(fcall, netdev_stats_cancelHandler);
	variant_initialize(&call->retval, variant_type_map);
	return call;
}

static void netdev_stats_call_finish(call_t *call) {
	SAH_TRACEZ_INFO(ME, "netdev_stats_call_finish(%s)", intf_name(call->intf));
	peer_info_t *peer = fcall_peer(call->fcall);
	request_t *request = fcall_request(call->fcall);
	pcb_replyBegin(peer, request, call->state);
	if (!call->state) {
		pcb_writeFunctionReturnBegin(peer, request);
		pcb_writeReturnValue(peer, request, &call->retval);
		pcb_writeFunctionReturnEnd(peer, request);
	}
	default_sendErrorList(peer, request);
	pcb_replyEnd(peer, request);
	request_setDone(request);
	netdev_stats_call_destroy(call);
}

static bool netdev_stats_replyHandler(request_t *req, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)req; (void)pcb; (void)from;
	call_t *call = (call_t *)userdata;
	reply_item_t *item;
	for (item = reply_firstItem(request_reply(req)); item; item = reply_nextItem(item)) {
		if (reply_item_type(item) == reply_type_object) {
			object_t *object = reply_item_object(item);
			parameter_t *parameter;
			for (parameter = object_firstParameter(object); parameter; parameter = object_nextParameter(parameter)) {
				variant_map_add(variant_da_map(&call->retval), parameter_name(parameter), parameter_getValue(parameter));
			}
		} else if (reply_item_type(item) == reply_type_error) {
			SAH_TRACE_WARNING("netdev - Received error for NetDevStats of %s : %08x - %s - %s", intf_name(call->intf),
			               reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
			reply_addError(request_reply(fcall_request(call->fcall)),
			               reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
			call->state = pcb_error_function_exec_failed;
		}
	}
	return true;
}

static bool netdev_stats_doneHandler(request_t *req, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)req; (void)pcb; (void)from;
	netdev_stats_call_finish((call_t *)userdata);
	return true;
}

static function_exec_state_t netdev_stats_getNetDevStats(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)args; (void)retval;
	function_exec_state_t state = function_exec_error;
	intf_t *intf = plugin_object2intf(fcall_object(fcall));
	call_t *call = NULL;
	int index = netdev_link_intf2index(intf);
	string_t path;
	string_initialize(&path, 64);
	if (index <= 0) {
		SAH_TRACEZ_INFO(ME, "NeMo.Intf.%s is not bound to any NetDev.Link", intf_name(intf));
		reply_addError(request_reply(fcall_request(fcall)), pcb_error_not_found,
		                             "Intf not bound to any NetDev.Link", intf_name(intf));
		goto leave;
	}
	string_appendFormat(&path, "NetDev.Link.%d.Stats", index);
	call = netdev_stats_call_create(intf, fcall);
	call->request = request_create_getObject(string_buffer(&path), 0, request_common_path_key_notation | request_no_object_caching
	                                         | request_getObject_parameters);
	request_setData(call->request, call);
	request_setReplyHandler(call->request, netdev_stats_replyHandler);
	request_setDoneHandler(call->request, netdev_stats_doneHandler);
	if (!pcb_sendRequest(plugin_getPcb(), dst, call->request)) {
		SAH_TRACE_WARNING("netdev - Failed to send request %s?", request_path(call->request));
		reply_addError(request_reply(fcall_request(fcall)), pcb_error_communication, "Failed to send request",
		               request_path(call->request));
		goto leave;
	}
	state = function_exec_executing;
leave:
	if (state != function_exec_executing)
		netdev_stats_call_destroy(call);
	string_cleanup(&path);
	return state;
}

static void netdev_stats_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata; (void)flag;
	if (value) {
		function_setHandler(object_getFunction(plugin_intf2object(intf), "getNetDevStats"), netdev_stats_getNetDevStats);
	} else {
		function_setHandler(object_getFunction(plugin_intf2object(intf), "getNetDevStats"), NULL);
	}
}

void netdev_stats_initialize(peer_info_t *dstref) {
	dst = dstref;
	intf_addFlagListener(NULL, "netdev", netdev_stats_rootHandler, NULL, true);
}

void netdev_stats_cleanup() {
	intf_delFlagListener(NULL, "netdev", netdev_stats_rootHandler, NULL, true);
	while (!llist_isEmpty(&calls))
		netdev_stats_call_destroy((call_t *)llist_first(&calls));
}
