/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <nemo/core.h>

static const char *object_parameterCharSource(object_t *object, const char *parameter) {
	const char *ret = string_buffer(variant_da_string(object_parameterValue(object, parameter)));
	return ret?ret:"";
}

static void variant_map_addParameter(variant_map_t *map, parameter_t *parameter) {
	variant_map_add(map, parameter_name(parameter), parameter_getValue(parameter));
}

static void variant_map_addAddr(variant_map_t *map, object_t *intf, object_t *addr, const char *family) {
	variant_map_addParameter(map, object_getParameter(intf, "NetDevIndex"));
	variant_map_addParameter(map, object_getParameter(intf, "NetDevName"));
	variant_map_addChar(map, "Family", family);
	variant_map_addParameter(map, object_getParameter(addr, "Address"));
	variant_map_addParameter(map, object_getParameter(addr, "Peer"));
	variant_map_addParameter(map, object_getParameter(addr, "PrefixLen"));
	variant_map_addParameter(map, object_getParameter(addr, "Flags"));
	variant_map_addParameter(map, object_getParameter(addr, "Scope"));
	variant_map_addChar(map, "NeMoIntf", object_name(intf, path_attr_key_notation));
	variant_map_addParameter(map, object_getParameter(addr, "NeMoFlags"));
}

typedef struct _netdev_api_family {
	const char *parent;
	const char *flag;
	const char *delegate;
} netdev_api_family_t;

static netdev_api_family_t netdev_api_ipv4 = { "IPv4Addr", "ipv4", NULL };
static netdev_api_family_t netdev_api_ipv6 = { "IPv6Addr", "ipv6", "IPv6AddrDelegate" };

typedef struct _netdev_api_ctx {
	variant_t *retval;
	flagexp_t *flag;
	traverse_mode_t traverse;
	netdev_api_family_t *family;
} netdev_api_ctx_t;

static bool netdev_api_getAddrsStep(intf_t *intf, netdev_api_ctx_t *ctx, intf_t *origintf) {
	bool done = false;
	object_t *intfobject = plugin_intf2object(intf);
	object_t *origintfobject = origintf ? plugin_intf2object(origintf) : intfobject;
	object_t *object, *parent = object_getObject(intfobject, ctx->family->parent, 0, NULL);
	object_for_each_instance(object, parent) {
		const char *status = object_parameterCharSource(object, "Status");
		if (strcmp(status, "bound") && strcmp(status, "dynamic"))
			continue; // only consider active addresses
		flagset_t *flagset = flagset_create(false);
		flagset_parse(flagset, object_parameterCharSource(object, "Flags"));
		flagset_set(flagset, ctx->family->flag);
		flagset_set(flagset, object_parameterCharSource(object, "Scope"));
		flagset_set(flagset, object_parameterCharSource(object, "NeMoFlags"));
		if (origintf && // special case: never delegate link-local or smaller scope addresses
				(flagset_check(flagset, "link") || flagset_check(flagset, "host") || flagset_check(flagset, "nowhere"))) {
			flagset_destroy(flagset);
			continue;
		}
		if (flagexp_evaluate(ctx->flag, flagset)) {
			switch (variant_type(ctx->retval)) {
				case variant_type_array: {
					variant_map_t map;
					variant_map_initialize(&map);
					variant_map_addAddr(&map, origintfobject, object, ctx->family->flag);
					variant_list_addMap(variant_da_list(ctx->retval), &map);
					variant_map_cleanup(&map);
					break;
				}
				case variant_type_map:
					variant_map_addAddr(variant_da_map(ctx->retval), origintfobject, object, ctx->family->flag);
					done = true;
					break;
				case variant_type_string:
					variant_setChar(ctx->retval, object_parameterCharSource(object, "Address"));
					done = true;
					break;
				default: break;
			}
		}
		flagset_destroy(flagset);
		if (done)
			break;
	}
	return done;
}

static bool netdev_api_getAddrsNode(traverse_node_t *node, netdev_api_ctx_t *ctx) {
	traverse_edge_t *edge;
	if (traverse_node_marker(node))
		return false;
	traverse_node_mark(node, 1);
	if (!traverse_node_intf(node))
		goto children;
	if (netdev_api_getAddrsStep(traverse_node_intf(node), ctx, NULL))
		return true;
	do { // handle address delegate
		if (!ctx->family->delegate)
			break;
		if (!flagset_check(intf_flagset(traverse_node_intf(node)), ctx->family->flag))
			break;
		const char *name = object_da_parameterCharValue(plugin_intf2object(traverse_node_intf(node)), ctx->family->delegate);
		if (!name)
			break;
		intf_t *intf = intf_find(name);
		if (!intf)
			break;
		if (netdev_api_getAddrsStep(intf, ctx, traverse_node_intf(node)))
			return true;
	} while (0);
	switch (ctx->traverse) {
	case traverse_mode_down:
	case traverse_mode_one_level_down:
	case traverse_mode_down_exclusive:
		if (flagset_check(intf_flagset(traverse_node_intf(node)), ctx->family->flag))
			return false;
	default:
		break;
	}
children:
	for (edge = traverse_node_firstEdge(node); edge; edge = traverse_edge_next(edge)) {
		if (netdev_api_getAddrsNode(traverse_edge_node(edge), ctx))
			return true;
	}
	return false;
}

static void netdev_api_getAddrsImpl(variant_t *retval, intf_t *intf, flagexp_t *flag, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	netdev_api_ctx_t ctx = {retval, flag, traverse, &netdev_api_ipv4};
	if (netdev_api_getAddrsNode(traverse_tree_root(tree), &ctx))
		goto leave;
	traverse_tree_clearMarks(tree);
	ctx.family = &netdev_api_ipv6;
	netdev_api_getAddrsNode(traverse_tree_root(tree), &ctx);
leave:
	traverse_tree_destroy(tree);
}

static function_exec_state_t netdev_api_getAddrs(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	variant_setType(retval, variant_type_array);
	netdev_api_getAddrsImpl(retval, plugin_object2intf(fcall_object(fcall)), flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

static function_exec_state_t netdev_api_luckyAddr(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	variant_setType(retval, variant_type_map);
	netdev_api_getAddrsImpl(retval, plugin_object2intf(fcall_object(fcall)), flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

static function_exec_state_t netdev_api_luckyAddrAddress(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	variant_setType(retval, variant_type_string);
	netdev_api_getAddrsImpl(retval, plugin_object2intf(fcall_object(fcall)), flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

struct _query_args {
	traverse_mode_t traverse;
	flagexp_t *flag;
};

static void netdev_api_query_args_destroy(query_class_t *cl, query_args_t *args) {
	(void)cl;
	if (!args)
		return;
	flagexp_destroy(args->flag);
	free(args);
}

static bool netdev_api_query_args_convert(query_class_t *cl, query_args_t **args, argument_value_list_t *arglist, uint32_t attributes) {
	*args = calloc(1, sizeof(query_args_t));
	(*args)->traverse = traverse_mode_down;
	if (!arg_takeFlagexp(&(*args)->flag, arglist, attributes, "flag"))
		goto error;
	if (!arg_takeTraverse(&(*args)->traverse, arglist, attributes, "traverse"))
		goto error;
	return true;
error:
	netdev_api_query_args_destroy(cl, *args);
	*args = NULL;
	return false;
}

static bool netdev_api_query_args_compare(query_class_t *cl, query_args_t *args1, query_args_t *args2) {
	(void)cl;
	return (args1->traverse == args2->traverse) && flagexp_equals(args1->flag, args2->flag);
}

static char *netdev_api_query_args_describe(query_class_t *cl, query_args_t *args) {
#define DESCRIPTIONLEN 64
	(void)cl;
	char *description = calloc(DESCRIPTIONLEN, 1);
	if (!description)
		return NULL;
	char *descptr = description;
	int n=0;
	if (DESCRIPTIONLEN > (descptr - description)) {
		char *flag = NULL;
		if (args->flag)
			flag = flagexp_print(args->flag);
		descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%sflag=\"%s\"", n++?", ":"", flag?flag:"");
		free(flag);
	}
	if (DESCRIPTIONLEN > (descptr - description))
		descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%straverse=\"%s\"", n++?", ":"", traverse_mode_string(args->traverse));
	(void)descptr; (void)n;
	return description;
}

static void netdev_api_query_run(query_t *q, variant_t *result) {
	query_args_t *args = query_args(q);
	variant_setType(result, query_class_info(query_class(q))->type);
	netdev_api_getAddrsImpl(result, query_intf(q), args->flag, args->traverse);
}

#define NETDEV_QUERY_CLASS_COUNT 3
static query_class_info_t netdev_api_query_classes_info[NETDEV_QUERY_CLASS_COUNT] = {
	{
		/* name */          "getAddrs",
		/* type */          variant_type_array,
		/* args_convert */  netdev_api_query_args_convert,
		/* args_destroy */  netdev_api_query_args_destroy,
		/* args_compare */  netdev_api_query_args_compare,
		/* args_describe */ netdev_api_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           netdev_api_query_run,
		/* userdata */      NULL,
	}, {
		/* name */          "luckyAddr",
		/* type */          variant_type_map,
		/* args_convert */  netdev_api_query_args_convert,
		/* args_destroy */  netdev_api_query_args_destroy,
		/* args_compare */  netdev_api_query_args_compare,
		/* args_describe */ netdev_api_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           netdev_api_query_run,
		/* userdata */      NULL,
	}, {
		/* name */          "luckyAddrAddress",
		/* type */          variant_type_string,
		/* args_convert */  netdev_api_query_args_convert,
		/* args_destroy */  netdev_api_query_args_destroy,
		/* args_compare */  netdev_api_query_args_compare,
		/* args_describe */ netdev_api_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           netdev_api_query_run,
		/* userdata */      NULL,
	}
};
static query_class_t *netdev_api_query_classes[NETDEV_QUERY_CLASS_COUNT] = {NULL, NULL, NULL};

static void netdev_api_intfMgmtHandler(intf_t *intf, void *userdata) {
	(void)intf; (void)userdata;
	int i;
	for (i=0; i<NETDEV_QUERY_CLASS_COUNT; i++) {
		query_class_t *class = netdev_api_query_classes[i];
		query_t *query;
		for (query = query_first(class); query; query = query_next(query)) {
			query_args_t *args = (query_args_t *)query_args(query);
			if (args->traverse == traverse_mode_all)
				query_invalidate(query);
		}
	}
}

static void netdev_api_intfCreateHandler(intf_t *intf, void *userdata) {
	(void)userdata;
	function_setHandler(object_getFunction(plugin_intf2object(intf), "getAddrs"), netdev_api_getAddrs);
	function_setHandler(object_getFunction(plugin_intf2object(intf), "luckyAddr"), netdev_api_luckyAddr);
	function_setHandler(object_getFunction(plugin_intf2object(intf), "luckyAddrAddress"), netdev_api_luckyAddrAddress);
	netdev_api_intfMgmtHandler(intf, userdata);
}

static void netdev_api_intfDestroyHandler(intf_t *intf, void *userdata) {
	(void)userdata;
	function_setHandler(object_getFunction(plugin_intf2object(intf), "getAddrs"), NULL);
	function_setHandler(object_getFunction(plugin_intf2object(intf), "luckyAddr"), NULL);
	function_setHandler(object_getFunction(plugin_intf2object(intf), "luckyAddrAddress"), NULL);
	netdev_api_intfMgmtHandler(intf, userdata);
}

static void netdev_api_intfLinkHandler(intf_t *ulintf, intf_t *llintf, bool value, void *userdata) {
	(void)ulintf; (void)llintf; (void)value; (void)userdata;
	int i;
	for (i=0; i<NETDEV_QUERY_CLASS_COUNT; i++) {
		query_class_t *class = netdev_api_query_classes[i];
		query_t *query;
		for (query = query_first(class); query; query = query_next(query)) {
			query_args_t *args = (query_args_t *)query_args(query);
			if (args->traverse != traverse_mode_this && args->traverse != traverse_mode_all)
				query_invalidate(query);
		}
	}
}

void netdev_api_query_invalidate() {
	int i;
	for (i=0; i<NETDEV_QUERY_CLASS_COUNT; i++)
		query_class_invalidate(netdev_api_query_classes[i]);
}

static void netdev_api_intfMibHandler(intf_t *intf, intf_mibevent_t event, const char *path, void *userdata) {
	(void)intf; (void)userdata;
	if (event != intf_mibevent_updated)
		return;
	if (strcmp(path, "IPv4AddrDelegate") && strcmp(path, "IPv6AddrDelegate"))
		return;
	netdev_api_query_invalidate();
}

void netdev_api_initialize() {
	int i;
	for (i=0; i<NETDEV_QUERY_CLASS_COUNT; i++) {
		netdev_api_query_classes[i] = query_class_register(&netdev_api_query_classes_info[i]);
	}
	intf_addMgmtListener(NULL, netdev_api_intfCreateHandler, NULL, netdev_api_intfDestroyHandler, NULL, true);
	intf_addMibListener(NULL, netdev_api_intfMibHandler, NULL);
	intf_addLinkListener(NULL, NULL, netdev_api_intfLinkHandler, NULL, false);
}

void netdev_api_cleanup() {
	intf_delLinkListener(NULL, NULL, netdev_api_intfLinkHandler, NULL, false);
	intf_delMibListener(NULL, netdev_api_intfMibHandler, NULL);
	intf_delMgmtListener(NULL, netdev_api_intfCreateHandler, NULL, netdev_api_intfDestroyHandler, NULL, true);
	int i;
	for (i=0; i<NETDEV_QUERY_CLASS_COUNT; i++) 
		query_class_unregister(netdev_api_query_classes[i]);
}
