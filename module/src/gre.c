/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <nemo/core.h>

typedef struct _intf_blob {
	intf_t *intf;
	char *type;
	nemo_query_t *linkquery;
	int32_t link;
	bool enabled;
	map_t *map;
} gre_t;

static peer_info_t *gre_dst = NULL;
static int gre_dummy;
static void *gre_key = &gre_dummy;

static void gre_translateType(map_parameter_t *parameter, map_direction_t direction, variant_t *value) {
	gre_t *gre = map_parameter_userdata(parameter);
	if (direction != map_to_destination)
		return;
	variant_setChar(value, gre->type);
}

static void gre_translateLink(map_parameter_t *parameter, map_direction_t direction, variant_t *value) {
	gre_t *gre = map_parameter_userdata(parameter);
	if (direction != map_to_destination)
		return;
	variant_setInt32(value, gre->link >= 0 ? gre->link : 0);
}

static void gre_down(gre_t *gre) {
	flagset_clear(intf_flagset(gre->intf), "up");
	map_destroy(gre->map);
	gre->map = NULL;
}

static void gre_afterGetReplyHandler(map_t *map, reply_item_t *item) {
	gre_t *gre = map_userdata(map);
	if (reply_item_type(item) == reply_type_error) {
		gre_down(gre);
	} else {
		flagset_set(intf_flagset(gre->intf), "up");
	}
}

static void gre_setup(gre_t *gre) {
	if (!gre->enabled) {
		gre_down(gre);
		return;
	}
	if (!gre->map) {
		object_t *object = plugin_intf2object(gre->intf);
		gre->map = map_create(object, MAP_OWNER, gre_dst, "NetDev.GRE.%s", intf_name(gre->intf));
		map_setUserdata(gre->map, gre);
		map_setAfterGetReplyHandler(gre->map, gre_afterGetReplyHandler);
		map_parameter_create(gre->map, NULL, "Type", 0, gre_translateType, gre);
		map_parameter_create(gre->map, NULL, "Link", 0, gre_translateLink, gre);
		map_parameter_create(gre->map, object_getParameter(object, "Local"), NULL, 0, NULL, NULL);
		map_parameter_create(gre->map, object_getParameter(object, "Remote"), NULL, 0, NULL, NULL);
		map_parameter_create(gre->map, object_getParameter(object, "IFlags"), NULL, 0, NULL, NULL);
		map_parameter_create(gre->map, object_getParameter(object, "OFlags"), NULL, 0, NULL, NULL);
		map_parameter_create(gre->map, object_getParameter(object, "IKey"), NULL, 0, NULL, NULL);
		map_parameter_create(gre->map, object_getParameter(object, "OKey"), NULL, 0, NULL, NULL);
		map_parameter_create(gre->map, object_getParameter(object, "TTL"), NULL, 0, NULL, NULL);
		map_parameter_create(gre->map, object_getParameter(object, "TOS"), NULL, 0, NULL, NULL);
		map_parameter_create(gre->map, object_getParameter(object, "PMTUDisc"), NULL, 0, NULL, NULL);
	}
	map_commit(gre->map);
}

static void gre_linkHandler(const variant_t *result, void *userdata) {
	gre_t *gre = userdata;
	gre->link = variant_int32(result);
	gre_setup(gre);
}

static void gre_enableHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)intf; (void)flag;
	gre_t *gre = userdata;
	gre->enabled = value;
	gre_setup(gre);
}

static void gre_modifyHandler(intf_t *intf, void *userdata) {
	(void)intf;
	gre_t *gre = userdata;
	gre_setup(gre);
}

static void gre_destroy(gre_t *gre) {
	if (!gre)
		return;
	gre_down(gre);
	intf_delFlagListener(gre->intf, "enabled", gre_enableHandler, gre, false);
	intf_delMgmtListener(gre->intf, NULL, gre_modifyHandler, NULL, gre, false);
	intf_delBlob(gre->intf, gre_key);
	nemo_closeQuery(gre->linkquery);
	free(gre->type);
	free(gre);
}

static gre_t *gre_create(intf_t *intf, const char *type) {
	gre_t *gre = calloc(1, sizeof(gre_t));
	if (!gre) {
		SAH_TRACE_ERROR("calloc() failed");
		goto error;
	}
	intf_addBlob(intf, gre_key, gre);
	gre->intf = intf;
	gre->type = strdup(type);
	if (!gre->type) {
		SAH_TRACE_ERROR("strdup(\"%s\") failed", type);
		goto error;
	}
	gre->linkquery = nemo_openQuery_getFirstParameter(intf_name(intf), "gre", "NetDevIndex", "netdev-bound", "one level down",
			gre_linkHandler, gre);
	intf_addFlagListener(intf, "enabled", gre_enableHandler, gre, true);
	intf_addMgmtListener(intf, NULL, gre_modifyHandler, NULL, gre, false);
	return gre;
error:
	gre_destroy(gre);
	return NULL;
}

static void gre_rootHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;
	gre_t *gre = intf_getBlob(intf, gre_key);
	if (value && !gre)
		gre_create(intf, flag);
	else if (!value && gre)
		gre_destroy(gre);
}

void gre_initialize(peer_info_t *dst) {
	gre_dst = dst;
	intf_addFlagListener(NULL, "gre", gre_rootHandler, NULL, true);
	intf_addFlagListener(NULL, "gretap", gre_rootHandler, NULL, true);
	intf_addFlagListener(NULL, "ip6gre", gre_rootHandler, NULL, true);
	intf_addFlagListener(NULL, "ip6gretap", gre_rootHandler, NULL, true);
}

void gre_cleanup() {
	intf_delFlagListener(NULL, "gre", gre_rootHandler, NULL, true);
	intf_delFlagListener(NULL, "gretap", gre_rootHandler, NULL, true);
	intf_delFlagListener(NULL, "ip6gre", gre_rootHandler, NULL, true);
	intf_delFlagListener(NULL, "ip6gretap", gre_rootHandler, NULL, true);
}
