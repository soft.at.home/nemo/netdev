INSTALL ?= install
COMPONENT=netdev

compile: 
	$(MAKE) -C module all
	$(MAKE) -C plugin all
	$(MAKE) -C scripts all

clean: 
	$(MAKE) -C module clean
	$(MAKE) -C plugin clean
	$(MAKE) -C scripts clean

install: 
	$(INSTALL) -d $(D)/usr/lib/$(COMPONENT)/
	$(INSTALL) -d $(D)/usr/lib/nemo/modules/
	$(INSTALL) -D -m 0755 scripts/$(COMPONENT).sh $(D)/etc/init.d/$(COMPONENT)
	$(INSTALL) -D -m 755 plugin/src/$(COMPONENT).so $(D)/usr/lib/$(COMPONENT)/
	$(INSTALL) -D -m 755 module/src/$(COMPONENT).so $(D)/usr/lib/nemo/modules/
	$(INSTALL) -D -m 644 odl/$(COMPONENT).odl $(D)/usr/lib/$(COMPONENT)/
	$(INSTALL) -m 0755 scripts/netcfg.sh $(D)/usr/lib/$(COMPONENT)/netcfg.sh

.PHONY: compile clean install


