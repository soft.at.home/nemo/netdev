/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <linux/if.h>
#include <linux/if_arp.h>
#ifdef CONFIG_WRITE_LINK_WITH_IOCTL
#include <sys/ioctl.h>
#include <linux/sockios.h>
#endif

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "util.h"
#include "rtnl.h"
#include "convert.h"
#include "link.h"
#include "route.h"
#include "procfs.h"

#ifndef IFF_NOMULTIPATH
#define IFF_NOMULTIPATH	0x80000		/* Disable for MPTCP 		*/
#endif

typedef struct _link {
	llist_iterator_t it;
	int index;
	unsigned short type;
	unsigned int flags;
	char name[LINKNAMESPACE];
	lladdr_t lladdr;
	unsigned int txqueuelen;
	unsigned int mtu;
	unsigned char state;
	object_t *object;
	pcb_timer_t *mercytimer;
	uint32_t seqthreshold;
} link_t;

static pcb_t *pcb = NULL;

static bool busy = false;

static llist_t links = {NULL, NULL};

static converter_t *type_converter = NULL;
static converter_t *flags_converter = NULL;
static converter_t *state_converter = NULL;

static llist_t link_infos = {NULL, NULL};

#ifdef CONFIG_WRITE_LINK_WITH_IOCTL
static int ioctlfd = -1;
#endif

static const lladdr_t *str2lladdrWrapperForXrefs(const char *value, bool *success) {
	if (!value || *value != '@')
		return str2lladdr(value, success);
	if (success) *success = false;
	// we have an xref + offset
	// parse xref
	char *expr = strdup(++value);
	char *name = expr;
	char tmpchar;
	while (*expr && *expr != ' ' && *expr != '+')
		expr++;
	tmpchar = *expr;
	*expr = '\0';
	// find link
	link_t *link;
	for (link = (link_t *)llist_first(&links); link; link = (link_t *)llist_iterator_next(&link->it)) {
		if (!strcmp(link->name, name))
			break;
	}
	if (!link) {
		SAH_TRACE_ERROR("link %s not found", name);
		goto leave;
	}
	*expr = tmpchar;
	// retrieve its lladdr
	static lladdr_t lladdr;
	lladdr_copy(&lladdr, &link->lladdr);

	// parse offset
	while (*expr == ' ') expr++;
	unsigned int offset = 0;
	while (*expr == '+') {
		expr++;
		offset += strtol(expr, &expr, 0);
		while (*expr == ' ') expr++;
	}

	SAH_TRACE_INFO("set LLAddress from xref [xref=%s base=%s offset=%d]", link->name, lladdr2str(&lladdr), offset);

	// add offset
	unsigned char *data;
	unsigned int tmpint;
	for (data = lladdr.data + lladdr.len - 1; data >= lladdr.data && offset; data--) {
		tmpint = offset + *data;
		*data = (unsigned char)tmpint;
		offset = tmpint >> 8;
	}
	if (success) *success = true;
leave:
	free(name);
	return &lladdr;
}

static void link_destroy(link_t *link) {
	if (!link)
		return;
	procfs_flush(link->index);
	pcb_timer_destroy(link->mercytimer);
	llist_iterator_take(&link->it);
	free(link);
}

static void link_expireMercy(pcb_timer_t *timer, void *userdata) {
	(void)timer;
	busy = true;
	link_t *link = (link_t *)userdata;
	object_delete(link->object);
	object_commit(link->object);
	link_destroy(link);
	busy = false;
}

static link_t *link_create(int index) {
	link_t *link = calloc(1, sizeof(link_t));
	link->index = index;
	link->mercytimer = pcb_timer_create();
	pcb_timer_setUserData(link->mercytimer, link);
	pcb_timer_setHandler(link->mercytimer, link_expireMercy);
	llist_append(&links, &link->it);
	return link;
}

static link_t *link_find(int index) {
	link_t *link;
	for (link = (link_t *)llist_first(&links); link; link = (link_t *)llist_iterator_next(&link->it)) {
		if (link->index == index)
			return link;
	}
	return NULL;
}

static void handleNewLink(rtnlmsg_t *msg) {
	struct ifinfomsg *ifinfo = (struct ifinfomsg *)rtnlmsg_hdr(msg);
	struct rtattr *rta;
	busy = true;
	link_t *link = link_find(ifinfo->ifi_index);
	if (link) {
		pcb_timer_stop(link->mercytimer);
		if (rtnlmsg_nlhdr(msg)->nlmsg_seq < link->seqthreshold)
			goto leave;
		else
			link->seqthreshold = 0;
	} else {
		link = link_create(ifinfo->ifi_index);
		object_t *parent = pcb_getObject(pcb, "NetDev.Link", 0);
		link->object = object_createInstance(parent, link->index, NULL);
		object_parameterSetInt32Value(link->object, "Index", link->index);
		object_setUserData(link->object, link);
	}
	link->type = ifinfo->ifi_type;
	object_parameterSetCharValue(link->object, "Type", convert_bin2str(type_converter, link->type, NULL));
	link->flags = ifinfo->ifi_flags;
	object_parameterSetCharValue(link->object, "Flags", convert_bin2str(flags_converter, link->flags, NULL));
	if (!(link->flags & IFF_UP))
		route_deleteIPv4Routes(link->index);
	rta = rtnlmsg_attr(msg, IFLA_IFNAME);
	if (rta) {
		snprintf(link->name, LINKNAMESPACE, "%s", (const char *)RTA_DATA(rta));
		object_parameterSetCharValue(link->object, "Name", link->name);
	}
	rta = rtnlmsg_attr(msg, IFLA_ADDRESS);
	if (rta) {
		lladdr_assign(&link->lladdr, RTA_PAYLOAD(rta), (unsigned char *)RTA_DATA(rta));
		object_parameterSetCharValue(link->object, "LLAddress", lladdr2str(&link->lladdr));
	}
	rta = rtnlmsg_attr(msg, IFLA_TXQLEN);
	if (rta) {
		link->txqueuelen = *(unsigned int *)RTA_DATA(rta);
		object_parameterSetUInt32Value(link->object, "TxQueueLen", link->txqueuelen);
	}
	rta = rtnlmsg_attr(msg, IFLA_MTU);
	if (rta) {
		link->mtu = *(unsigned int *)RTA_DATA(rta);
		object_parameterSetUInt32Value(link->object, "MTU", link->mtu);
	}
	rta = rtnlmsg_attr(msg, IFLA_OPERSTATE);
	if (rta) {
		link->state = *(unsigned char *)RTA_DATA(rta);
		object_parameterSetCharValue(link->object, "State", convert_bin2str(state_converter, link->state, NULL));
	}
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
	rta = rtnlmsg_attr(msg, IFLA_STATS64);
#else
	rta = rtnlmsg_attr(msg, IFLA_STATS);
#endif
	if (rta) {
		object_t *object = object_getObject(link->object, "Stats", 0, NULL);
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
		struct rtnl_link_stats64 *stats = (struct rtnl_link_stats64 *)RTA_DATA(rta);
#else
		struct rtnl_link_stats *stats = (struct rtnl_link_stats *)RTA_DATA(rta);
#endif
		object_parameterSetUInt64Value(object, "RxPackets", stats->rx_packets);
		object_parameterSetUInt64Value(object, "TxPackets", stats->tx_packets);
		object_parameterSetUInt64Value(object, "RxBytes", stats->rx_bytes);
		object_parameterSetUInt64Value(object, "TxBytes", stats->tx_bytes);
		object_parameterSetUInt64Value(object, "RxErrors", stats->rx_errors);
		object_parameterSetUInt64Value(object, "TxErrors", stats->tx_errors);
		object_parameterSetUInt64Value(object, "RxDropped", stats->rx_dropped);
		object_parameterSetUInt64Value(object, "TxDropped", stats->tx_dropped);
		object_parameterSetUInt64Value(object, "Multicast", stats->multicast);
		object_parameterSetUInt64Value(object, "Collisions", stats->collisions);
		object_parameterSetUInt64Value(object, "RxLengthErrors", stats->rx_length_errors);
		object_parameterSetUInt64Value(object, "RxOverErrors", stats->rx_over_errors);
		object_parameterSetUInt64Value(object, "RxCrcErrors", stats->rx_crc_errors);
		object_parameterSetUInt64Value(object, "RxFrameErrors", stats->rx_frame_errors);
		object_parameterSetUInt64Value(object, "RxFifoErrors", stats->rx_fifo_errors);
		object_parameterSetUInt64Value(object, "RxMissedErrors", stats->rx_missed_errors);
		object_parameterSetUInt64Value(object, "TxAbortedErrors", stats->tx_aborted_errors);
		object_parameterSetUInt64Value(object, "TxCarrierErrors", stats->tx_carrier_errors);
		object_parameterSetUInt64Value(object, "TxFifoErrors", stats->tx_fifo_errors);
		object_parameterSetUInt64Value(object, "TxHeartbeatErrors", stats->tx_heartbeat_errors);
		object_parameterSetUInt64Value(object, "TxWindowErrors", stats->tx_window_errors);
	}
	object_commit(link->object);
	procfs_kick(link->index, link->name);
leave:
	busy = false;
}

static void handleDelLink(rtnlmsg_t *msg) {
	struct ifinfomsg *ifinfo = (struct ifinfomsg *)rtnlmsg_hdr(msg);
	link_t *link = link_find(ifinfo->ifi_index);
	if (!link)
		return;
	// Delete links with mercy - on the target it happens that we get a DELLINK while the link is still there.
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_GETLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST);
	ifinfo = (struct ifinfomsg *)rtnlmsg_hdr(msg);
	ifinfo->ifi_index = link->index;
	rtnl_send(msg);
	pcb_timer_start(link->mercytimer, 100);
}

static struct rtattr *nestedAttr(struct rtattr *rta, unsigned short type) {
	int rtalen = RTA_PAYLOAD(rta);
	for (rta = RTA_DATA(rta); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
		if (rta->rta_type == type)
			return rta;
	}
	return NULL;
}

static void printLinkMsg(FILE *f, rtnlmsg_t *msg) {
	fprintf(f, "%s{",
		rtnlmsg_type(msg) == RTM_NEWLINK ? "RTM_NEWLINK" :
		rtnlmsg_type(msg) == RTM_DELLINK ? "RTM_DELLINK" :
		rtnlmsg_type(msg) == RTM_GETLINK ? "RTM_GETLINK" : "<UNKNOWN>");
	struct ifinfomsg *ifinfo = (struct ifinfomsg *)rtnlmsg_hdr(msg);
	fprintf(f, " index=%d type=%d(%s)", ifinfo->ifi_index, ifinfo->ifi_type, convert_bin2str(type_converter, ifinfo->ifi_type, NULL));
	fprintf(f, " flags=0x%x(%s)", ifinfo->ifi_flags, convert_bin2str(flags_converter, ifinfo->ifi_flags, NULL));
	fprintf(f, " change=0x%x(%s)", ifinfo->ifi_change, convert_bin2str(flags_converter, ifinfo->ifi_change, NULL));
	bool breakout = false;
	struct rtattr *rta;
	unsigned int rtalen = rtnlmsg_attrLen(msg);
	for (rta = rtnlmsg_attrFirst(msg); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
		switch (rta->rta_type) {
			case IFLA_IFNAME:    fprintf(f, " name=%s",      (const char *)RTA_DATA(rta)); break;
			case IFLA_ADDRESS:   fprintf(f, " lladdress=%s", lladdr2str(bin2lladdr(RTA_PAYLOAD(rta), (unsigned char *)RTA_DATA(rta)))); break;
			case IFLA_BROADCAST: fprintf(f, " broadcast=%s", lladdr2str(bin2lladdr(RTA_PAYLOAD(rta), (unsigned char *)RTA_DATA(rta)))); break;
			case IFLA_TXQLEN:    fprintf(f, " txqlen=%u",   *(unsigned int *)RTA_DATA(rta)); break;
			case IFLA_MTU:       fprintf(f, " mtu=%u",      *(unsigned int *)RTA_DATA(rta)); break;
			case IFLA_QDISC:     fprintf(f, " qdisc=%s",     (const char *)RTA_DATA(rta)); break;
			case IFLA_MAP:       fprintf(f, " map"); break;
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
			case IFLA_STATS64:   fprintf(f, " stats"); break;
#else
			case IFLA_STATS:     fprintf(f, " stats"); break;
#endif
			case IFLA_LINKMODE:  fprintf(f, " linkmode=%u",  *(unsigned char *)RTA_DATA(rta)); break;
			case IFLA_OPERSTATE: {
				unsigned char operstate = *(unsigned char *)RTA_DATA(rta);
				fprintf(f, " operstate=%u(%s)", operstate, convert_bin2str(state_converter, operstate, NULL));
				break;
			}
			case IFLA_LINKINFO: {
				struct rtattr *kind = nestedAttr(rta, IFLA_INFO_KIND);
				struct rtattr *data = nestedAttr(rta, IFLA_INFO_DATA);
				if (!kind || !data)
					break;
				link_info_t *info;
				for (info = (link_info_t *)llist_first(&link_infos); info; info = (link_info_t *)llist_iterator_next(&info->it))
					if (!strcmp(info->kind, RTA_DATA(kind)))
						break;
				fprintf(f, " %s=", (const char *)RTA_DATA(kind));
				if (info && info->print)
					info->print(f, data);
				else
					fprintf(f, "(%u byte%s)", RTA_PAYLOAD(data), RTA_PAYLOAD(data)==1?"":"s");
				break;
			}
			default: {
				fprintf(f, " attr[%u](%u byte%s)", rta->rta_type, RTA_PAYLOAD(rta), RTA_PAYLOAD(rta)==1?"":"s");
				if (rta->rta_type > IFLA_MAX) {
					SAH_TRACE_ERROR("Got rta with unknown rta_type=%u > IFLA_MAX=%u (rtnlmsg=%p rta=%p)", rta->rta_type, IFLA_MAX, msg, rta);
					breakout = true;
				}
				break;
			}
		}

		if (breakout) {
				break;
		}
	}
	fprintf(f, " }");
}

static void writeLink(object_t *object) {
	if (busy)
		return;
	link_t *link = (link_t *)object_getUserData(object);
	if (!link)
		return;
	rtnlmsg_t *msg = rtnlmsg_instance();
	struct ifinfomsg *ifinfo;
	link_t ws;
	ws.flags = convert_str2bin(flags_converter, object_parameterCharSource(object, "Flags"), NULL);
	const char *name = object_parameterCharSource(object, "Name");
	const lladdr_t *lladdr = str2lladdrWrapperForXrefs(object_parameterCharSource(object, "LLAddress"), NULL);
	ws.txqueuelen = object_parameterUInt32Value(object, "TxQueueLen");
	ws.mtu = object_parameterUInt32Value(object, "MTU");
#ifdef CONFIG_WRITE_LINK_WITH_IOCTL // use ioctl i.s.o. rtnetlink
	struct ifreq ifr;
	memset(&ifr, 0, sizeof(struct ifreq));

	if ((link->flags & IFF_UP) && (
			strcmp(link->name, name) ||
			(lladdr->len != link->lladdr.len || memcmp(lladdr->data, link->lladdr.data, lladdr->len)))) {
		// interface must be down for setting the name or MAC-address
		strcpy(ifr.ifr_name, link->name);
		ifr.ifr_flags = link->flags & ~IFF_UP;
		if (ioctl(ioctlfd, SIOCSIFFLAGS, &ifr) < 0)
			SAH_TRACE_ERROR("ioctl(SIOCSIFFLAGS, ifr{name=%s flags=0x%x}) failed: %m", ifr.ifr_name, ifr.ifr_flags);
		link->flags = link->flags & ~IFF_UP;
	}

	if (strcmp(link->name, name)) {
		strcpy(ifr.ifr_name, link->name);
		strcpy(ifr.ifr_newname, name);
		if (ioctl(ioctlfd, SIOCSIFNAME, &ifr) < 0)
			SAH_TRACE_ERROR("ioctl(SIOCSIFNAME, ifr{name=%s newname=%s}) failed: %m", ifr.ifr_name, ifr.ifr_newname);
		snprintf(link->name, LINKNAMESPACE, "%s", name);
	}

	if (lladdr->len != link->lladdr.len || memcmp(lladdr->data, link->lladdr.data, lladdr->len)) {
		strcpy(ifr.ifr_name, link->name);
		ifr.ifr_hwaddr.sa_family = link->type;
		memcpy(ifr.ifr_hwaddr.sa_data, lladdr->data, lladdr->len);
		if (ioctl(ioctlfd, SIOCSIFHWADDR, &ifr) < 0)
			SAH_TRACE_ERROR("ioctl(SIOCSIFHWADDR, ifr{name=%s hwaddr.family=%u hwaddr.data=%s}) failed: %m"
					, ifr.ifr_name, ifr.ifr_hwaddr.sa_family, lladdr2str(bin2lladdr(lladdr->len, lladdr->data)));
		link->lladdr.len = lladdr->len;
		memcpy(link->lladdr.data, lladdr->data, lladdr->len);
	}

	if (ws.txqueuelen != link->txqueuelen) {
		strcpy(ifr.ifr_name, link->name);
		ifr.ifr_qlen = ws.txqueuelen;
		if (ioctl(ioctlfd, SIOCSIFTXQLEN, &ifr) < 0)
			SAH_TRACE_ERROR("ioctl(SIOCSIFTXQLEN, ifr{name=%s qlen=%u}) failed: %m", ifr.ifr_name, ifr.ifr_qlen);
		link->txqueuelen = ws.txqueuelen;
	}

	if (ws.mtu != link->mtu) {
		strcpy(ifr.ifr_name, link->name);
		ifr.ifr_mtu = ws.mtu;
		if (ioctl(ioctlfd, SIOCSIFMTU, &ifr) < 0)
			SAH_TRACE_ERROR("ioctl(SIOCSIFMTU, ifr{name=%s mtu=%u}) failed: %m", ifr.ifr_name, ifr.ifr_mtu);
		link->mtu = ws.mtu;
	}

	if (ws.flags != link->flags) {
		strcpy(ifr.ifr_name, link->name);
		ifr.ifr_flags = ws.flags;
		if (ioctl(ioctlfd, SIOCSIFFLAGS, &ifr) < 0)
			SAH_TRACE_ERROR("ioctl(SIOCSIFFLAGS, ifr{name=%s flags=0x%x}) failed: %m", ifr.ifr_name, ifr.ifr_flags);
		link->flags = ws.flags;
	}
#else // standard implementation
	if ((link->flags & IFF_UP) && strcmp(link->name, name)) {
		// interface must be down for setting the name
		rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_NEWLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST);
		ifinfo = (struct ifinfomsg *)rtnlmsg_hdr(msg);
		ifinfo->ifi_index = link->index;
		ifinfo->ifi_flags = link->flags & ~IFF_UP;
		ifinfo->ifi_change = IFF_UP;
		link->flags = link->flags & ~IFF_UP;
		rtnl_send(msg);
	}
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_NEWLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST);
	ifinfo = (struct ifinfomsg *)rtnlmsg_hdr(msg);
	ifinfo->ifi_index = link->index;
	if (ws.flags != link->flags) {
		ifinfo->ifi_flags = ws.flags;
		ifinfo->ifi_change = ws.flags ^ link->flags;
		link->flags = ws.flags;
	}
	if (strcmp(link->name, name)) {
		snprintf(link->name, LINKNAMESPACE, "%s", name);
		rtnlmsg_addAttr(msg, IFLA_IFNAME, strlen(name) + 1, link->name);
	}
	if (lladdr->len != link->lladdr.len || memcmp(lladdr->data, link->lladdr.data, lladdr->len)) {
		link->lladdr.len = lladdr->len;
		memcpy(link->lladdr.data, lladdr->data, lladdr->len);
		rtnlmsg_addAttr(msg, IFLA_ADDRESS, link->lladdr.len, link->lladdr.data);
	}
	if (ws.txqueuelen != link->txqueuelen) {
		link->txqueuelen = ws.txqueuelen;
		rtnlmsg_addAttr(msg, IFLA_TXQLEN, sizeof(unsigned int), &ws.txqueuelen);
	}
	if (ws.mtu != link->mtu) {
		link->mtu = ws.mtu;
		rtnlmsg_addAttr(msg, IFLA_MTU, sizeof(unsigned int), &ws.mtu);
	}
	rtnl_send(msg);
#endif
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_GETLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST);
	ifinfo = (struct ifinfomsg *)rtnlmsg_hdr(msg);
	ifinfo->ifi_index = link->index;
	rtnl_send(msg);
	link->seqthreshold = rtnlmsg_nlhdr(msg)->nlmsg_seq;
}

static bool readLink(object_t *object) {
	int index = strtol(object_name(object, 0), NULL, 0);
	if (index <= 0)
		return true;
	rtnlmsg_t *msg = rtnlmsg_instance();
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_GETLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST);
	struct ifinfomsg *ifinfo = (struct ifinfomsg *)rtnlmsg_hdr(msg);
	ifinfo->ifi_index = index;
	if (!rtnl_send(msg))
		return true;
	rtnl_read(msg, true);
	return true;
}

static bool readStats(object_t *object) {
	return readLink(object_parent(object));
}

static bool validateLinkFlags(parameter_t *parameter, void *validationData) {
	(void)validationData;
	bool success;
	convert_str2bin(flags_converter, parameter_getModifiedCharSource(parameter), &success);
	return success;
}

static rtnlhandler_t newLinkHandler = { RTM_NEWLINK, sizeof(struct ifinfomsg), handleNewLink, printLinkMsg };
static rtnlhandler_t delLinkHandler = { RTM_DELLINK, sizeof(struct ifinfomsg), handleDelLink, printLinkMsg };
static rtnlhandler_t getLinkHandler = { RTM_GETLINK, sizeof(struct ifinfomsg), NULL,          printLinkMsg };

bool link_initialize(pcb_t *pcbref) {
	pcb = pcbref;

	object_t *object = pcb_getObject(pcb, "NetDev.Link", 0);
	object_setWriteHandler(object, writeLink);
    object_setReadHandler(object, readLink);
	parameter_setValidator(object_getParameter(object, "Flags"), param_validator_create_custom(validateLinkFlags, NULL));
	object = object_getObject(object, "Stats", 0, NULL);
	object_setReadHandler(object, readStats);

	type_converter = converter_create("link_type", converter_attribute_fallback);
	converter_loadEntry(type_converter, ARPHRD_ETHER, "ether");
	converter_loadEntry(type_converter, ARPHRD_ATM, "atm");
	converter_loadEntry(type_converter, ARPHRD_VOID, "void");
	converter_loadEntry(type_converter, ARPHRD_NONE, "none");
	converter_loadEntry(type_converter, ARPHRD_PPP, "ppp");
	converter_loadEntry(type_converter, ARPHRD_TUNNEL, "tunnel");
	converter_loadEntry(type_converter, ARPHRD_TUNNEL6, "tunnel6");
	converter_loadEntry(type_converter, ARPHRD_LOOPBACK, "loopback");

	flags_converter = converter_create("link_flags", converter_attribute_flags);
	converter_loadEntry(flags_converter, IFF_LOOPBACK, "loopback");
	converter_loadEntry(flags_converter, IFF_BROADCAST, "broadcast");
	converter_loadEntry(flags_converter, IFF_POINTOPOINT, "pointopoint");
	converter_loadEntry(flags_converter, IFF_MULTICAST, "multicast");
	converter_loadEntry(flags_converter, IFF_NOARP, "noarp");
	converter_loadEntry(flags_converter, IFF_ALLMULTI, "allmulti");
	converter_loadEntry(flags_converter, IFF_PROMISC, "promisc");
	converter_loadEntry(flags_converter, IFF_MASTER, "master");
	converter_loadEntry(flags_converter, IFF_SLAVE, "slave");
	converter_loadEntry(flags_converter, IFF_DEBUG, "debug");
	converter_loadEntry(flags_converter, IFF_DYNAMIC, "dynamic");
	converter_loadEntry(flags_converter, IFF_AUTOMEDIA, "automedia");
	converter_loadEntry(flags_converter, IFF_PORTSEL, "portsel");
	converter_loadEntry(flags_converter, IFF_NOTRAILERS, "notrailers");
	converter_loadEntry(flags_converter, IFF_UP, "up");
	converter_loadEntry(flags_converter, IFF_NOMULTIPATH, "nomultipath");

	state_converter = converter_create("link_state", converter_attribute_fallback);
	converter_loadEntry(state_converter, 0, "unknown"); // binary values borrowed from kernel documentation
	converter_loadEntry(state_converter, 1, "notpresent");
	converter_loadEntry(state_converter, 2, "down");
	converter_loadEntry(state_converter, 3, "lowerlayerdown");
	converter_loadEntry(state_converter, 4, "testing");
	converter_loadEntry(state_converter, 5, "dormant");
	converter_loadEntry(state_converter, 6, "up");

	rtnl_registerHandler(&newLinkHandler);
	rtnl_registerHandler(&delLinkHandler);
	rtnl_registerHandler(&getLinkHandler);

#ifdef CONFIG_WRITE_LINK_WITH_IOCTL
	ioctlfd = socket(PF_INET, SOCK_DGRAM, 0);
#endif
	return true;
}

bool link_start() {
	rtnlmsg_t *msg = rtnlmsg_instance();
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_GETLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST | NLM_F_DUMP);
	if (!rtnl_send(msg))
		return false;
	if (!rtnl_read(msg, true))
		return false;
	return true;
}

void link_cleanup() {
	rtnl_unregisterHandler(&newLinkHandler);
	rtnl_unregisterHandler(&delLinkHandler);
	rtnl_unregisterHandler(&getLinkHandler);

	while (!llist_isEmpty(&links))
		link_destroy((link_t *)llist_first(&links));

	converter_destroy(type_converter);
	converter_destroy(flags_converter);
	converter_destroy(state_converter);
#ifdef CONFIG_WRITE_LINK_WITH_IOCTL
	close(ioctlfd);
#endif
}

void link_register_info(link_info_t *info) {
	llist_append(&link_infos, &info->it);
}

void link_unregister_info(link_info_t *info) {
	llist_iterator_take(&info->it);
}
