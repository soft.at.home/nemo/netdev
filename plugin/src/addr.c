/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "util.h"
#include "rtnl.h"
#include "convert.h"
#include "addr.h"
#include "route.h"

typedef struct _addr {
	llist_iterator_t it;
	int link;
	unsigned char family;
	unsigned char prefixlen;
	unsigned char flags;
	unsigned char scope;
	unsigned char address[ADDRSPACE];
	unsigned char peer[ADDRSPACE];
	unsigned long preferred_lifetime;
	unsigned long valid_lifetime;
	unsigned long created_timestamp;
	unsigned long updated_timestamp;
	object_t *object;
	pcb_timer_t *mercytimer;
} addr_t;

static pcb_t *pcb = NULL;

static bool busy = false;

static llist_t addrs = {NULL, NULL};

static converter_t *flags_converter = NULL;
static converter_t *scope_converter = NULL;

static unsigned char *rtnlmsg_address(rtnlmsg_t *msg) {
	struct rtattr *rta = rtnlmsg_attr(msg, IFA_LOCAL);
	if (!rta)
		rta = rtnlmsg_attr(msg, IFA_ADDRESS);
	return rta?(unsigned char *)RTA_DATA(rta):NULL;
}

static unsigned char *rtnlmsg_peer(rtnlmsg_t *msg) {
	struct rtattr *rta_address = NULL, *rta_local = rtnlmsg_attr(msg, IFA_LOCAL);
	if (rta_local)
		rta_address = rtnlmsg_attr(msg, IFA_ADDRESS);
	if (rta_local && rta_address && 
			!memcmp(RTA_DATA(rta_local), RTA_DATA(rta_address), addrlen(((struct ifaddrmsg *)rtnlmsg_hdr(msg))->ifa_family)))
		rta_address = NULL;
	return rta_address?(unsigned char *)RTA_DATA(rta_address):NULL;
}

static void addr_destroy(addr_t *addr, bool deleteObject) {
	if (!addr)
		return;
	if (addr->object)
		object_setUserData(addr->object, NULL);
	if (deleteObject && addr->object) {
		busy = true;
		object_delete(addr->object);
		object_commit(addr->object);
		busy = false;
	}
	pcb_timer_destroy(addr->mercytimer);
	llist_iterator_take(&addr->it);
	free(addr);
}

static void addr_expireMercy(pcb_timer_t *timer, void *userdata) {
	(void)timer;
	addr_destroy((addr_t *)userdata, true);
}

static addr_t *addr_create() {
	addr_t *addr = calloc(1, sizeof(addr_t));
	addr->mercytimer = pcb_timer_create();
	pcb_timer_setUserData(addr->mercytimer, addr);
	pcb_timer_setHandler(addr->mercytimer, addr_expireMercy);
	llist_iterator_initialize(&addr->it);
	llist_append(&addrs, &addr->it);
	return addr;
}

static void addr_copy(addr_t *dst, addr_t *src) {
	dst->link = src->link;
	dst->family = src->family;
	dst->prefixlen = src->prefixlen;
	dst->flags = src->flags;
	dst->scope = src->scope;
	memcpy(dst->address, src->address, addrlen(dst->family));
	memcpy(dst->peer, src->peer, addrlen(dst->family));
	dst->preferred_lifetime = src->preferred_lifetime;
	dst->valid_lifetime = src->valid_lifetime;
	dst->created_timestamp = src->created_timestamp;
	dst->updated_timestamp = src->updated_timestamp;
}

static addr_t *addr_find(addr_t *ref) {
	addr_t *addr;
	for (addr = (addr_t *)llist_first(&addrs); addr; addr = (addr_t *)llist_iterator_next(&addr->it)) {
		if (addr->link == ref->link && addr->family == ref->family 
				&& !memcmp(addr->address, ref->address, addrlen(ref->family)))
			return addr;
	}
	return NULL;
}

static void addr2msg(rtnlmsg_t *msg, addr_t *addr) {
	unsigned long now = (unsigned long)uptime();
	struct ifaddrmsg *ifaddr = (struct ifaddrmsg *)rtnlmsg_hdr(msg);
	ifaddr->ifa_family = addr->family;
	ifaddr->ifa_prefixlen = addr->prefixlen;
	ifaddr->ifa_flags = addr->flags;
	ifaddr->ifa_scope = addr->scope;
	ifaddr->ifa_index = addr->link;
	rtnlmsg_addAttr(msg, IFA_LOCAL, addrlen(addr->family), addr->address);
	if (!addr_isNull(addr->family, addr->peer))
		rtnlmsg_addAttr(msg, IFA_ADDRESS, addrlen(addr->family), addr->peer);
	struct ifa_cacheinfo cacheinfo;
	memset(&cacheinfo, 0, sizeof(struct ifa_cacheinfo));
	if (addr->preferred_lifetime == 0)
		cacheinfo.ifa_prefered = 0xffffffff;
	else if (addr->preferred_lifetime <= now)
		cacheinfo.ifa_prefered = 0;
	else
		cacheinfo.ifa_prefered = addr->preferred_lifetime - now;
	if (addr->valid_lifetime == 0)
		cacheinfo.ifa_valid = 0xffffffff;
	else if (addr->valid_lifetime <= now)
		cacheinfo.ifa_valid = 0;
	else
		cacheinfo.ifa_valid = addr->valid_lifetime - now;
	rtnlmsg_addAttr(msg, IFA_CACHEINFO, sizeof(struct ifa_cacheinfo), &cacheinfo);
}

static void msg2addr(addr_t *addr, rtnlmsg_t *msg) {
	memset(addr, 0, sizeof(addr_t));
	struct ifaddrmsg *ifaddr = (struct ifaddrmsg *)rtnlmsg_hdr(msg);
	addr->family = ifaddr->ifa_family;
	addr->prefixlen = ifaddr->ifa_prefixlen;
	addr->flags = ifaddr->ifa_flags;
	addr->scope = ifaddr->ifa_scope;
	addr->link = ifaddr->ifa_index;
	unsigned char *address = rtnlmsg_address(msg);
	if (address)
		memcpy(addr->address, address, addrlen(addr->family));
	unsigned char *peer = rtnlmsg_peer(msg);
	if (peer)
		memcpy(addr->peer, peer, addrlen(addr->family));
	struct rtattr *rta = rtnlmsg_attr(msg, IFA_CACHEINFO);
	if (rta) {
		unsigned long now = (unsigned long)uptime();
		struct ifa_cacheinfo *cacheinfo = (struct ifa_cacheinfo *)RTA_DATA(rta);
		addr->preferred_lifetime = cacheinfo->ifa_prefered == 0xffffffff ? 0 : now + cacheinfo->ifa_prefered;
		addr->valid_lifetime = cacheinfo->ifa_valid == 0xffffffff ? 0 : now + cacheinfo->ifa_valid;
		addr->created_timestamp = cacheinfo->cstamp / 100;
		addr->updated_timestamp = cacheinfo->tstamp / 100;
	}
}

static void handleNewAddr(rtnlmsg_t *msg) {
	busy = true;
	addr_t ws;
	msg2addr(&ws, msg);
	addr_t *addr = addr_find(&ws);
	if (addr) {
		pcb_timer_stop(addr->mercytimer);
	} else {
		char pathbuf[40];
		sprintf(pathbuf, "NetDev.Link.%d.%sAddr", ws.link, family2str(ws.family));
		object_t *parent = pcb_getObject(pcb, pathbuf, 0);
		if (!parent) {
			SAH_TRACE_ERROR("NetDev.Link.%d.%sAddr not found!", ws.link, family2str(ws.family));
			goto leave;
		}
		addr = addr_create();
		sprintf(pathbuf, "dyn%u", ({static unsigned counter = 0; counter++;}));
		addr->object = object_createInstance(parent, 0, pathbuf);
		object_setUserData(addr->object, addr);
	}
	addr_copy(addr, &ws);
	object_parameterSetCharValue(addr->object, "Address", addr2str(addr->family, addr->address, false));
	object_parameterSetCharValue(addr->object, "Peer", addr2str(addr->family, addr->peer, true));
	object_parameterSetUInt32Value(addr->object, "PrefixLen", addr->prefixlen);
	object_parameterSetCharValue(addr->object, "Flags", convert_bin2str(flags_converter, addr->flags, NULL));
	object_parameterSetCharValue(addr->object, "Scope", convert_bin2str(scope_converter, addr->scope, NULL));
	object_parameterSetCharValue(addr->object, "NeMoFlags", addr2flag(addr->family, addr->address, false));
	object_parameterSetUInt32Value(addr->object, "PreferredLifetime", addr->preferred_lifetime);
	object_parameterSetUInt32Value(addr->object, "ValidLifetime", addr->valid_lifetime);
	object_parameterSetUInt32Value(addr->object, "CreatedTimestamp", addr->created_timestamp);
	object_parameterSetUInt32Value(addr->object, "UpdatedTimestamp", addr->updated_timestamp);
	object_commit(addr->object);
leave:
	busy = false;
}

static void handleDelAddr(rtnlmsg_t *msg) {
	addr_t ws;
	msg2addr(&ws, msg);
	addr_t *addr = addr_find(&ws);
	if (!addr)
		return;
	pcb_timer_start(addr->mercytimer, 100);

	// check if all ipv4-routes have to be deleted implicitly
	if (ws.family == AF_INET) {
		addr_t *a;
		for (a = (addr_t *)llist_first(&addrs); a; a = (addr_t *)llist_iterator_next(&a->it)) {
			if (a->link == ws.link && a->family == AF_INET && a != addr)
				break;
		}
		if (!a) // last ipv4-address was deleted -> delete routes implicitly
			route_deleteIPv4Routes(ws.link);
	}
}

static void printAddrMsg(FILE *f, rtnlmsg_t *msg) {
	fprintf(f, "%s{", 
			rtnlmsg_type(msg) == RTM_NEWADDR ? "RTM_NEWADDR" :
			rtnlmsg_type(msg) == RTM_DELADDR ? "RTM_DELADDR" :
			rtnlmsg_type(msg) == RTM_GETADDR ? "RTM_GETADDR" : "<UNKNOWN>");
	struct ifaddrmsg *ifaddr = (struct ifaddrmsg *)rtnlmsg_hdr(msg);	
	fprintf(f, " family=%u(%s)", ifaddr->ifa_family, family2str(ifaddr->ifa_family));
	fprintf(f, " prefixlen=%u", ifaddr->ifa_prefixlen);
	fprintf(f, " flags=0x%x(%s)", ifaddr->ifa_flags, convert_bin2str(flags_converter, ifaddr->ifa_flags, NULL));
	fprintf(f, " scope=%u(%s)", ifaddr->ifa_scope, convert_bin2str(scope_converter, ifaddr->ifa_scope, NULL));
	fprintf(f, " index=%u", ifaddr->ifa_index);
	struct rtattr *rta;
	unsigned int rtalen = rtnlmsg_attrLen(msg);
	for (rta = rtnlmsg_attrFirst(msg); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
		switch (rta->rta_type) {
		case IFA_ADDRESS:   fprintf(f, " address=%s",   addr2str(ifaddr->ifa_family, (unsigned char *)RTA_DATA(rta), false)); break;
		case IFA_LOCAL:     fprintf(f, " local=%s",     addr2str(ifaddr->ifa_family, (unsigned char *)RTA_DATA(rta), false)); break;
		case IFA_BROADCAST: fprintf(f, " broadcast=%s", addr2str(ifaddr->ifa_family, (unsigned char *)RTA_DATA(rta), false)); break;
		case IFA_ANYCAST:   fprintf(f, " anycast=%s",   addr2str(ifaddr->ifa_family, (unsigned char *)RTA_DATA(rta), false)); break;
		case IFA_LABEL:     fprintf(f, " label=%s",     (const char *)RTA_DATA(rta)); break;
		case IFA_CACHEINFO: {
			struct ifa_cacheinfo *ci = (struct ifa_cacheinfo *)RTA_DATA(rta);
			fprintf(f, " cacheinfo{ preferred=%lu valid=%lu cstamp=%lu tstamp=%lu }",
					(unsigned long)ci->ifa_prefered, (unsigned long)ci->ifa_valid,
					(unsigned long)ci->cstamp, (unsigned long)ci->tstamp);
			break;
		}
		default:
			fprintf(f, " attr[%u](%u byte%s)", rta->rta_type, RTA_PAYLOAD(rta), RTA_PAYLOAD(rta)==1?"":"s");
			break;
		}
	}
	fprintf(f, " }");
}

static void writeAddr(object_t *object) {
	if (busy)
		return;
	rtnlmsg_t *msg = rtnlmsg_instance();
	addr_t ws;
	memset(&ws, 0, sizeof(addr_t));
	bool success;
	ws.link = strtol(object_name(object_parent(object_parent(object)), 0), NULL, 0);
	if (!ws.link)
		return;
	ws.family = object_family(object);
	memcpy(ws.address, str2addr(ws.family, object_parameterCharSource(object, "Address"), &success), addrlen(ws.family));
	memcpy(ws.peer, str2addr(ws.family, object_parameterCharSource(object, "Peer"), NULL), addrlen(ws.family));
	ws.prefixlen = object_parameterUInt8Value(object, "PrefixLen");
	ws.flags = convert_str2bin(flags_converter, object_parameterCharSource(object, "Flags"), NULL);
	ws.scope = convert_str2bin(scope_converter, object_parameterCharSource(object, "Scope"), NULL);
	ws.preferred_lifetime = object_parameterUInt32Value(object, "PreferredLifetime");
	ws.valid_lifetime = object_parameterUInt32Value(object, "ValidLifetime");
	addr_t *addr = (addr_t *)object_getUserData(object);
	if (addr && (addr->link != ws.link || addr->family != ws.family
				 || memcmp(addr->address, ws.address, addrlen(ws.family))
				 || memcmp(addr->peer, ws.peer, addrlen(ws.family))
				 || addr->scope != ws.scope || addr->prefixlen != ws.prefixlen || addr->flags != ws.flags)) {
		rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_DELADDR, sizeof(struct ifaddrmsg), NLM_F_REQUEST);
		addr2msg(msg, addr);
		rtnl_send(msg);
		addr_destroy(addr, false);
		addr = NULL;
	}
	if (addr_isNull(ws.family, ws.address))
		return;
	if (!addr) {
		while ((addr = addr_find(&ws))) {
			rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_DELADDR, sizeof(struct ifaddrmsg), NLM_F_REQUEST);
			addr2msg(msg, addr);
			rtnl_send(msg);
			addr_destroy(addr, true);
			rtnl_read(msg, true);
		}
		addr = addr_create();
		addr->object = object;
		object_setUserData(object, addr);
	}
	addr_copy(addr, &ws);

	// If IPv6 is disabled, don't push through netlink
	if (addr->family == AF_INET6 && object_parameterBoolValue(object_parent(object_parent(object)), "IPv6Disable")) {
		pcb_timer_start(addr->mercytimer, 0);
		return;
	}

	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_NEWADDR, sizeof(struct ifaddrmsg), NLM_F_REQUEST | NLM_F_CREATE | NLM_F_REPLACE);
	addr2msg(msg, addr);
	rtnl_send(msg);
	// No events received for self-created IPv6-address, so beg the kernel for mercy
	if (addr->family == AF_INET6) {
		rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_GETADDR, sizeof(struct ifaddrmsg), NLM_F_REQUEST);
		addr2msg(msg, addr);
		rtnl_send(msg);
	}
	pcb_timer_start(addr->mercytimer, 2000);
}

static bool deleteAddr(object_t *object, object_t *instance) {
	(void)object;
	if (busy)
		return true;
	addr_t *addr = (addr_t *)object_getUserData(instance);
	if (!addr)
		return true;
	rtnlmsg_t *msg = rtnlmsg_instance();
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_DELADDR, sizeof(struct ifaddrmsg), NLM_F_REQUEST);
	addr2msg(msg, addr);
	rtnl_send(msg);
	addr_destroy(addr, false);
	return true;
}

static bool validateAddrAddr(parameter_t *parameter, void *validationData) {
	(void)validationData;
	bool success;
	str2addr(object_family(parameter_owner(parameter)), parameter_getModifiedCharSource(parameter), &success);
	return success;
}

static rtnlhandler_t newAddrHandler = { RTM_NEWADDR, sizeof(struct ifaddrmsg), handleNewAddr, printAddrMsg };
static rtnlhandler_t delAddrHandler = { RTM_DELADDR, sizeof(struct ifaddrmsg), handleDelAddr, printAddrMsg };
static rtnlhandler_t getAddrHandler = { RTM_GETADDR, sizeof(struct ifaddrmsg), NULL,          printAddrMsg };

bool addr_initialize(pcb_t *pcbref) {
	pcb = pcbref;
	
	flags_converter = converter_create("addr_flags", converter_attribute_flags);
	converter_loadEntry(flags_converter, IFA_F_SECONDARY, "secondary");
	converter_loadEntry(flags_converter, IFA_F_NODAD, "nodad");
	converter_loadEntry(flags_converter, IFA_F_OPTIMISTIC, "optimistic");
	converter_loadEntry(flags_converter, IFA_F_HOMEADDRESS, "homeaddress");
	converter_loadEntry(flags_converter, IFA_F_DEPRECATED, "deprecated");
	converter_loadEntry(flags_converter, IFA_F_TENTATIVE, "tentative");
	converter_loadEntry(flags_converter, IFA_F_PERMANENT, "permanent");
	converter_loadEntry(flags_converter, IFA_F_DADFAILED, "dadfailed");
	
	scope_converter = converter_create("addr_scope", converter_attribute_fallback);
	converter_loadEntry(scope_converter, RT_SCOPE_UNIVERSE, "global");
	converter_loadEntry(scope_converter, RT_SCOPE_SITE, "site");
	converter_loadEntry(scope_converter, RT_SCOPE_HOST, "host");
	converter_loadEntry(scope_converter, RT_SCOPE_LINK, "link");
	converter_loadEntry(scope_converter, RT_SCOPE_NOWHERE, "nowhere");
	converter_loadTable(scope_converter, pcb_getObject(pcb, "NetDev.ConversionTable.Scope", 0));

	const char *path[] = {"NetDev.Link.IPv4Addr", "NetDev.Link.IPv6Addr"};
	int i;
	for (i=0; i<2; i++) {
		object_t *object = pcb_getObject(pcb, path[i], 0);
		object_setWriteHandler(object, writeAddr);
		object_setInstanceDelHandler(object, deleteAddr);
		parameter_setValidator(object_getParameter(object, "Flags"), param_validator_create_custom(validateConverterParameter, flags_converter));
		parameter_setValidator(object_getParameter(object, "Scope"), param_validator_create_custom(validateConverterParameter, scope_converter));
		parameter_setValidator(object_getParameter(object, "Address"), param_validator_create_custom(validateAddrAddr, NULL));
		parameter_setValidator(object_getParameter(object, "Peer"), param_validator_create_custom(validateAddrAddr, NULL));
	}

	rtnl_registerHandler(&newAddrHandler);
	rtnl_registerHandler(&delAddrHandler);
	rtnl_registerHandler(&getAddrHandler);
	return true;
}

bool addr_start() {
	rtnlmsg_t *msg = rtnlmsg_instance();
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_GETADDR, sizeof(struct ifaddrmsg), NLM_F_REQUEST | NLM_F_DUMP);
	if (!rtnl_send(msg))
		return false;
	if (!rtnl_read(msg, true))
		return false;
	return true;
}

void addr_cleanup() {
	rtnl_unregisterHandler(&newAddrHandler);
	rtnl_unregisterHandler(&delAddrHandler);
	rtnl_unregisterHandler(&getAddrHandler);

	while (!llist_isEmpty(&addrs))
		addr_destroy((addr_t *)llist_first(&addrs), false);
	
	converter_destroy(flags_converter);
	converter_destroy(scope_converter);
}

