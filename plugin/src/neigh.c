/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "util.h"
#include "rtnl.h"
#include "convert.h"
#include "neigh.h"

typedef struct _neigh {
	llist_iterator_t it;
	// key start
	unsigned char family;
	int link;
	unsigned char flags;
	unsigned char dst[ADDRSPACE];
	// key end
	lladdr_t lladdr;
	unsigned short state;
	object_t *object;
} neigh_t;

static pcb_t *pcb = NULL;

static bool busy = false;

static llist_t neighs = {NULL, NULL};

static converter_t *family_converter = NULL;
static converter_t *flags_converter = NULL;
static converter_t *state_converter = NULL;

static void neigh_destroy(neigh_t *neigh, bool deleteObject) {
	if (!neigh)
		return;
	if (neigh->object)
		object_setUserData(neigh->object, NULL);
	if (deleteObject && neigh->object) {
		busy = true;
		const char *state = object_da_parameterCharValue(neigh->object, "State");
		if(!state || strcmp(state, "failed") != 0) {
			//Ensure than state is set to failed before deleting object.
			object_parameterSetCharValue(neigh->object, "State", "failed");
			object_commit(neigh->object);
		}
		object_delete(neigh->object);
		object_commit(neigh->object);
		busy = false;
	}
	llist_iterator_take(&neigh->it);
	free(neigh);
}

static neigh_t *neigh_create() {
	neigh_t *neigh = calloc(1, sizeof(neigh_t));
	llist_iterator_initialize(&neigh->it);
	llist_append(&neighs, &neigh->it);
	return neigh;
}

static void neigh_copy(neigh_t *dst, neigh_t *src) {
	dst->family = src->family;
	dst->link = src->link;
	dst->state = src->state;
	dst->flags = src->flags;
	memcpy(dst->dst, src->dst, addrlen(src->family));
	lladdr_copy(&dst->lladdr, &src->lladdr);
}

static bool neigh_match(neigh_t *n1, neigh_t *n2) {
	return 
		n1->family == n2->family && n1->link == n2->link && n1->flags == n2->flags &&
		!memcmp(n1->dst, n2->dst, addrlen(n1->family));
}

static neigh_t *neigh_find(neigh_t *ref) {
	neigh_t *neigh;
	for (neigh = (neigh_t *)llist_first(&neighs); neigh; neigh = (neigh_t *)llist_iterator_next(&neigh->it)) {
		if (neigh_match(neigh, ref))
			return neigh;
	}
	return NULL;
}

static void neigh2msg(rtnlmsg_t *msg, neigh_t *neigh) {
	struct ndmsg *nd = (struct ndmsg *)rtnlmsg_hdr(msg);
	nd->ndm_family = neigh->family;
	nd->ndm_ifindex = neigh->link;
	nd->ndm_state = neigh->state;
	nd->ndm_flags = neigh->flags;
	if (!addr_isNull(neigh->family, neigh->dst))
		rtnlmsg_addAttr(msg, NDA_DST, addrlen(neigh->family), neigh->dst);
	if (neigh->lladdr.len)
		rtnlmsg_addAttr(msg, NDA_LLADDR, neigh->lladdr.len, neigh->lladdr.data);
}

static void msg2neigh(neigh_t *neigh, rtnlmsg_t *msg) {
	memset(neigh, 0, sizeof(neigh_t));
	struct ndmsg *nd = (struct ndmsg *)rtnlmsg_hdr(msg);
	neigh->family = nd->ndm_family;
	neigh->link = nd->ndm_ifindex;
	neigh->state = nd->ndm_state;
	neigh->flags = nd->ndm_flags;
	struct rtattr *rta;
	if ((rta = rtnlmsg_attr(msg, NDA_DST)))
		memcpy(neigh->dst, RTA_DATA(rta), addrlen(neigh->family));
	if ((rta = rtnlmsg_attr(msg, NDA_LLADDR)))
		lladdr_assign(&neigh->lladdr, RTA_PAYLOAD(rta), (unsigned char *)RTA_DATA(rta));
}

static void handleNewNeigh(rtnlmsg_t *msg) {
	busy = true;
	// Don't show multicast/broadcast/... entries
	struct ndmsg *nd = (struct ndmsg *)rtnlmsg_hdr(msg);
	if (nd->ndm_type != RTN_UNICAST)
		return;
	neigh_t ws;
	msg2neigh(&ws, msg);
	neigh_t *neigh = neigh_find(&ws);
	if (neigh) {
		neigh_copy(neigh, &ws);
	} else {
		char pathbuf[40];
		sprintf(pathbuf, "NetDev.Link.%d.Neigh", ws.link);
		object_t *parent = pcb_getObject(pcb, pathbuf, 0);
		if (!parent) {
			SAH_TRACE_ERROR("NetDev.Link.%d.Neigh not found", ws.link);
			goto leave;
		}
		neigh = neigh_create();
		sprintf(pathbuf, "dyn%u", ({static unsigned counter = 0; counter++;}));
		neigh->object = object_createInstance(parent, 0, pathbuf);
		object_setUserData(neigh->object, neigh);
	}
	neigh_copy(neigh, &ws);
	object_parameterSetCharValue(neigh->object, "Family", convert_bin2str(family_converter, neigh->family, NULL));
	object_parameterSetCharValue(neigh->object, "Flags", convert_bin2str(flags_converter, neigh->flags, NULL));
	object_parameterSetCharValue(neigh->object, "Dst", addr2str(neigh->family, neigh->dst, false));
	object_parameterSetCharValue(neigh->object, "LLAddress", lladdr2str(&neigh->lladdr));
	object_parameterSetCharValue(neigh->object, "State", convert_bin2str(state_converter, neigh->state, NULL));
	object_commit(neigh->object);
leave:
	busy = false;
}

static void handleDelNeigh(rtnlmsg_t *msg) {
	neigh_t ws;
	msg2neigh(&ws, msg);
	neigh_t *neigh = neigh_find(&ws);
	if (!neigh)
		return;
	neigh_destroy(neigh, true);
}

static void printNeighMsg(FILE *f, rtnlmsg_t *msg) {
	fprintf(f, "%s{", 
		rtnlmsg_type(msg) == RTM_NEWNEIGH ? "RTM_NEWNEIGH" :
		rtnlmsg_type(msg) == RTM_DELNEIGH ? "RTM_DELNEIGH" :
		rtnlmsg_type(msg) == RTM_GETNEIGH ? "RTM_GETNEIGH" : "<UNKNOWN>");
	struct ndmsg *nd = (struct ndmsg *)rtnlmsg_hdr(msg);
	fprintf(f, " family=%u(%s)", nd->ndm_family, nd->ndm_family?convert_bin2str(family_converter, nd->ndm_family, NULL):"unspec");
	fprintf(f, " ifindex=%d", nd->ndm_ifindex);
	fprintf(f, " state=%u(%s)", nd->ndm_state, convert_bin2str(state_converter, nd->ndm_state, NULL));
	fprintf(f, " flags=%u(%s)", nd->ndm_flags, convert_bin2str(flags_converter, nd->ndm_flags, NULL));
	fprintf(f, " type=%u", nd->ndm_type);
	struct rtattr *rta;
	unsigned int rtalen = rtnlmsg_attrLen(msg);
	for (rta = rtnlmsg_attrFirst(msg); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
		switch (rta->rta_type) {
			case NDA_DST:    fprintf(f, " dst=%s",    addr2str(nd->ndm_family, (unsigned char *)RTA_DATA(rta), false)); break;
			case NDA_LLADDR: fprintf(f, " lladdr=%s", lladdr2str(bin2lladdr(RTA_PAYLOAD(rta), (unsigned char *)RTA_DATA(rta)))); break;
			case NDA_CACHEINFO: {
				struct nda_cacheinfo *ci = (struct nda_cacheinfo *)RTA_DATA(rta);
				fprintf(f, " cacheinfo{confirmed=%lu used=%lu updated=%lu refcnt=%lu}",
					(unsigned long)ci->ndm_confirmed, (unsigned long)ci->ndm_used, 
					(unsigned long)ci->ndm_updated, (unsigned long)ci->ndm_refcnt);
				break;
			}
			default:
				fprintf(f, " attr[%u](%u byte%s)", rta->rta_type, RTA_PAYLOAD(rta), RTA_PAYLOAD(rta)==1?"":"s"); 
				break;
		}
	}
	fprintf(f, " }");
}

static void writeNeigh(object_t *object) {
	if (busy)
		return;
	neigh_t ws;
	memset(&ws, 0, sizeof(neigh_t));
	rtnlmsg_t *msg = rtnlmsg_instance();
	ws.family = convert_str2bin(family_converter, object_parameterCharSource(object, "Family"), NULL);
	ws.flags = convert_str2bin(flags_converter, object_parameterCharSource(object, "Flags"), NULL);
	memcpy(ws.dst, str2addr(ws.family, object_parameterCharSource(object, "Dst"), NULL), addrlen(ws.family));
	lladdr_copy(&ws.lladdr, str2lladdr(object_parameterCharSource(object, "LLAddress"), NULL));
	ws.state = convert_str2bin(state_converter, object_parameterCharSource(object, "State"), NULL);
	ws.link = strtol(object_name(object_parent(object_parent(object)), 0), NULL, 0);
	if (!ws.link)
		return;
	
	neigh_t *neigh = object_getUserData(object);
	if (neigh && !neigh_match(&ws, neigh)) {
		rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_DELNEIGH, sizeof(struct ndmsg), NLM_F_REQUEST);
		neigh2msg(msg, neigh);
		rtnl_send(msg);
		neigh_destroy(neigh, false);
		neigh = NULL;
	}
	if (!neigh) {
		while ((neigh = neigh_find(&ws)))
			neigh_destroy(neigh, true);
		neigh = neigh_create();
	}
	neigh_copy(neigh, &ws);
	neigh->object = object;
	object_setUserData(object, neigh);
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_NEWNEIGH, sizeof(struct ndmsg), NLM_F_REQUEST | NLM_F_CREATE | NLM_F_REPLACE);
	neigh2msg(msg, neigh);
	rtnl_send(msg);
}

static bool deleteNeigh(object_t *object, object_t *instance) {
	(void)object;
	neigh_t *neigh = object_getUserData(instance);
	if (neigh) {
		rtnlmsg_t *msg = rtnlmsg_instance();
		rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_DELNEIGH, sizeof(struct ndmsg), NLM_F_REQUEST);
		neigh2msg(msg, neigh);
		rtnl_send(msg);
		neigh_destroy(neigh, false);
	}
	return true;
}

static bool validateNeighAddr(parameter_t *parameter, void *validationData) {
	(void)validationData;
	bool success;
	str2addr(convert_str2bin(family_converter, object_parameterModifiedCharSource(parameter_owner(parameter), "Family"), NULL)
			, parameter_getModifiedCharSource(parameter), &success);
	return success;
}

static bool validateNeighLLAddr(parameter_t *parameter, void *validationData) {
	(void)validationData;
	bool success;
	str2lladdr(parameter_getModifiedCharSource(parameter), &success);
	return success;
}

static rtnlhandler_t newNeighHandler = { RTM_NEWNEIGH, sizeof(struct ndmsg), handleNewNeigh, printNeighMsg };
static rtnlhandler_t delNeighHandler = { RTM_DELNEIGH, sizeof(struct ndmsg), handleDelNeigh, printNeighMsg };
static rtnlhandler_t getNeighHandler = { RTM_GETNEIGH, sizeof(struct ndmsg), NULL,           printNeighMsg };

bool neigh_initialize(pcb_t *pcbref) {
	pcb = pcbref;
	
	family_converter = converter_create("family", 0);
	converter_loadEntry(family_converter, AF_INET, "ipv4");
	converter_loadEntry(family_converter, AF_INET6, "ipv6");		
		
	flags_converter = converter_create("neigh_flags", converter_attribute_flags);
	converter_loadEntry(flags_converter, NTF_PROXY, "proxy");
	converter_loadEntry(flags_converter, NTF_ROUTER, "router");
	
	state_converter = converter_create("neigh_state", converter_attribute_flags);
	converter_loadEntry(state_converter, NUD_INCOMPLETE, "incomplete");
	converter_loadEntry(state_converter, NUD_REACHABLE, "reachable");
	converter_loadEntry(state_converter, NUD_STALE, "stale");
	converter_loadEntry(state_converter, NUD_DELAY, "delay");
	converter_loadEntry(state_converter, NUD_PROBE, "probe");
	converter_loadEntry(state_converter, NUD_FAILED, "failed");
	converter_loadEntry(state_converter, NUD_NOARP, "noarp");
	converter_loadEntry(state_converter, NUD_PERMANENT, "permanent");

	object_t *object = pcb_getObject(pcb, "NetDev.Link.Neigh", 0);
	object_setWriteHandler(object, writeNeigh);
	object_setInstanceDelHandler(object, deleteNeigh);
	parameter_setValidator(object_getParameter(object, "Family"), param_validator_create_custom(validateConverterParameter, family_converter));
	parameter_setValidator(object_getParameter(object, "Flags"), param_validator_create_custom(validateConverterParameter, flags_converter));
	parameter_setValidator(object_getParameter(object, "State"), param_validator_create_custom(validateConverterParameter, state_converter));
	parameter_setValidator(object_getParameter(object, "Dst"), param_validator_create_custom(validateNeighAddr, NULL));
	parameter_setValidator(object_getParameter(object, "LLAddress"), param_validator_create_custom(validateNeighLLAddr, NULL));

	rtnl_registerHandler(&newNeighHandler);
	rtnl_registerHandler(&delNeighHandler);
	rtnl_registerHandler(&getNeighHandler);
	return true;
}

bool neigh_start() {
	rtnlmsg_t *msg = rtnlmsg_instance();
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_GETNEIGH, sizeof(struct ndmsg), NLM_F_REQUEST | NLM_F_DUMP);
	if (!rtnl_send(msg))
		return false;
	if (!rtnl_read(msg, true))
		return false;
	return true;
}

void neigh_cleanup() {
	rtnl_unregisterHandler(&newNeighHandler);
	rtnl_unregisterHandler(&delNeighHandler);
	rtnl_unregisterHandler(&getNeighHandler);

	while (!llist_isEmpty(&neighs))
		neigh_destroy((neigh_t *)llist_first(&neighs), false);
	
	converter_destroy(family_converter);
	converter_destroy(flags_converter);
	converter_destroy(state_converter);
}

