/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/rtnetlink.h>
#include <linux/if.h>
#include <linux/ip.h>
#include <linux/if_tunnel.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "util.h"
#include "rtnl.h"
#include "convert.h"
#include "link.h"
#include "gre.h"

#ifndef IFLA_GRE_MAX
#warning "################################################################"
#warning "#### GRE tunnel support can not be compiled due to lack of #####"
#warning "#### support in <linux/if_tunnel.h>.                       #####"
#warning "#### Fix your toolchain if you want to set up GRE tunnels! #####"
#warning "################################################################"
#define DISABLE_GRE_SUPPORT
#endif

static pcb_t *pcb = NULL;

static converter_t *gre_flags_converter = NULL;
static converter_t *gre_ttl_converter = NULL;
static converter_t *gre_tos_converter = NULL;

#ifndef DISABLE_GRE_SUPPORT
static void gre_print(int family, FILE *f, struct rtattr *data) {
	int datalen = RTA_PAYLOAD(data);
	fprintf(f, "{");
	for (data = RTA_DATA(data); RTA_OK(data, datalen); data = RTA_NEXT(data, datalen)) {
		switch (data->rta_type) {
		case IFLA_GRE_LINK:     fprintf(f, " link=%d", *(int32_t *)RTA_DATA(data)); break;
		case IFLA_GRE_IFLAGS:   fprintf(f, " iflags=0x%x(%s)", *(uint16_t *)RTA_DATA(data), convert_bin2str(gre_flags_converter, *(uint16_t *)RTA_DATA(data), NULL)); break;
		case IFLA_GRE_OFLAGS:   fprintf(f, " oflags=0x%x(%s)", *(uint16_t *)RTA_DATA(data), convert_bin2str(gre_flags_converter, *(uint16_t *)RTA_DATA(data), NULL)); break;
		case IFLA_GRE_IKEY:     fprintf(f, " ikey=0x%x", ntohl(*(uint32_t *)RTA_DATA(data))); break;
		case IFLA_GRE_OKEY:     fprintf(f, " okey=0x%x", ntohl(*(uint32_t *)RTA_DATA(data))); break;
		case IFLA_GRE_LOCAL:    fprintf(f, " local=%s", addr2str(family, RTA_DATA(data), false)); break;
		case IFLA_GRE_REMOTE:   fprintf(f, " remote=%s", addr2str(family, RTA_DATA(data), false)); break;
		case IFLA_GRE_TTL:      fprintf(f, " ttl=%s", convert_bin2str(gre_ttl_converter, *(uint8_t *)RTA_DATA(data), NULL)); break;
		case IFLA_GRE_TOS:      fprintf(f, " tos=%s", convert_bin2str(gre_tos_converter, *(uint8_t *)RTA_DATA(data), NULL)); break;
		case IFLA_GRE_PMTUDISC: fprintf(f, " %sptmudisc", *(uint8_t *)RTA_DATA(data) ? "" : "no"); break;
		default:
			fprintf(f, " attr[%u](%u byte%s)", data->rta_type, RTA_PAYLOAD(data), RTA_PAYLOAD(data)==1?"":"s");
			break;
		}
	}
	fprintf(f, " }");
}

static void gre_ipv4_print(FILE *f, struct rtattr *data) {
	gre_print(AF_INET, f, data);
}

static void gre_ipv6_print(FILE *f, struct rtattr *data) {
	gre_print(AF_INET6, f, data);
}

static union {
	unsigned char buf[124]; // 124 is exactly the theortical maximum length
	struct rtattr rta;
} gre_link_info;

static unsigned char *gre_addAttr(unsigned char *bufptr, unsigned short type, int len, const void *data, struct rtattr **path,
								  struct rtattr **newrta) {
	struct rtattr *rta= (struct rtattr *)bufptr;
	rta->rta_type = type;
	rta->rta_len = RTA_LENGTH(len);
	bufptr += RTA_LENGTH(0);
	if (len > 0)
		memcpy(bufptr, data, len);
	bufptr += NLMSG_ALIGN(len);
	if (path) {
		for (; *path; path++)
			(*path)->rta_len = NLMSG_ALIGN((*path)->rta_len) + rta->rta_len;
	}
	if (newrta)
		*newrta = rta;
	return bufptr;
}

static void gre_new(object_t *object) {
	rtnlmsg_t *msg = rtnlmsg_instance();
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_NEWLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL);
	const char *name = object_name(object, path_attr_key_notation);
	rtnlmsg_addAttr(msg, IFLA_IFNAME, strlen(name) + 1, name);

	unsigned char *bufptr = gre_link_info.buf;
	struct rtattr *path[2] = {NULL, NULL};

	const char *type = object_da_parameterCharValue(object, "Type");
	bufptr = gre_addAttr(bufptr, IFLA_INFO_KIND, strlen(type) + 1, type, path, NULL);

	bufptr = gre_addAttr(bufptr, IFLA_INFO_DATA, 0, NULL, path, &path[0]);

	bool success;
	int32_t i32; uint32_t u32; uint8_t u8; uint16_t u16;
	int family = strncmp(type, "ip6", 3) ? AF_INET : AF_INET6;
	unsigned char *addr = NULL;

	i32 = object_parameterInt32Value(object, "Link");
	bufptr = gre_addAttr(bufptr, IFLA_GRE_LINK, sizeof(int32_t), &i32, path, NULL);

	u16 = convert_str2bin(gre_flags_converter, object_da_parameterCharValue(object, "IFlags"), NULL);
	bufptr = gre_addAttr(bufptr, IFLA_GRE_IFLAGS, sizeof(uint16_t), &u16, path, NULL);

	u16 = convert_str2bin(gre_flags_converter, object_da_parameterCharValue(object, "OFlags"), NULL);
	bufptr = gre_addAttr(bufptr, IFLA_GRE_OFLAGS, sizeof(uint16_t), &u16, path, NULL);

	u32 = htonl(object_parameterUInt32Value(object, "IKey"));
	bufptr = gre_addAttr(bufptr, IFLA_GRE_IKEY, sizeof(uint32_t), &u32, path, NULL);

	u32 = htonl(object_parameterUInt32Value(object, "OKey"));
	bufptr = gre_addAttr(bufptr, IFLA_GRE_OKEY, sizeof(uint32_t), &u32, path, NULL);

	addr = str2addr(family, object_da_parameterCharValue(object, "Local"), &success);
	if (!success)
		return;
	bufptr = gre_addAttr(bufptr, IFLA_GRE_LOCAL, addrlen(family), addr, path, NULL);

	addr = str2addr(family, object_da_parameterCharValue(object, "Remote"), &success);
	if (!success)
		return;
	bufptr = gre_addAttr(bufptr, IFLA_GRE_REMOTE, addrlen(family), addr, path, NULL);

	u8 = convert_str2bin(gre_ttl_converter, object_da_parameterCharValue(object, "TTL"), NULL);
	bufptr = gre_addAttr(bufptr, IFLA_GRE_TTL, sizeof(uint8_t), &u8, path, NULL);

	u8 = convert_str2bin(gre_tos_converter, object_da_parameterCharValue(object, "TOS"), NULL);
	bufptr = gre_addAttr(bufptr, IFLA_GRE_TOS, sizeof(uint8_t), &u8, path, NULL);

	u8 = object_parameterBoolValue(object, "PMTUDisc");
	gre_addAttr(bufptr, IFLA_GRE_PMTUDISC, sizeof(uint8_t), &u8, path, NULL);

	rtnlmsg_addAttr(msg, IFLA_LINKINFO, sizeof(gre_link_info), &gre_link_info);

	rtnl_send(msg);
}

static void gre_del(object_t *object) {
	rtnlmsg_t *msg = rtnlmsg_instance();
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_DELLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST);
	const char *name = object_name(object, path_attr_key_notation);
	rtnlmsg_addAttr(msg, IFLA_IFNAME, strlen(name) + 1, name);
	rtnl_send(msg);
}
#else
static void gre_ipv4_print(FILE *f, struct rtattr *data) {
	(void)data;
	fprintf(f, "(GRE tunnel support disabled at compile time)");
}
static void gre_ipv6_print(FILE *f, struct rtattr *data) {
	(void)data;
	fprintf(f, "(GRE tunnel support disabled at compile time)");
}
static void gre_new(object_t *object) {
	SAH_TRACE_ERROR("GRE tunnel %s can not be created: GRE support is disabled at compile time!",
			object_name(object, path_attr_key_notation));
}
static void gre_del(object_t *object) {(void)object;}
#endif

static void gre_writeObject(object_t *object) {
	if (!object_isInstance(object))
		return;
	if (object_getUserData(object))
		gre_del(object);
	gre_new(object);
	object_setUserData(object, pcb);
}

static bool gre_delInstance(object_t *object, object_t *instance) {
	object = instance;
	if (object_getUserData(object))
		gre_del(object);
	return true;
}


static bool gre_validateAddr(parameter_t *parameter, void *validationData) {
	(void)validationData;
	unsigned char addr[ADDRSPACE];
	return (parameter_getModifiedCharSource(parameter) == NULL || *parameter_getModifiedCharSource(parameter) == 0 ||
			inet_pton(AF_INET, parameter_getModifiedCharSource(parameter), addr) == 1 ||
			inet_pton(AF_INET6, parameter_getModifiedCharSource(parameter), addr) == 1);
}

static link_info_t gre_gre_info = {
	.kind = "gre",
	.print = gre_ipv4_print,
};

static link_info_t gre_gretap_info = {
	.kind = "gretap",
	.print = gre_ipv4_print,
};

static link_info_t gre_ip6gre_info = {
	.kind = "ip6gre",
	.print = gre_ipv6_print,
};

static link_info_t gre_ip6gretap_info = {
	.kind = "ip6gretap",
	.print = gre_ipv6_print,
};

void gre_initialize(pcb_t *pcbref) {
	pcb = pcbref;

	gre_flags_converter = converter_create("gre_flags", converter_attribute_flags);
#ifndef DISABLE_GRE_SUPPORT
	converter_loadEntry(gre_flags_converter, GRE_SEQ, "seq");
	converter_loadEntry(gre_flags_converter, GRE_CSUM, "csum");
	converter_loadEntry(gre_flags_converter, GRE_KEY, "key");
#else
	converter_loadEntry(gre_flags_converter, 0, "seq");
	converter_loadEntry(gre_flags_converter, 1, "csum");
	converter_loadEntry(gre_flags_converter, 2, "key");
#endif

	gre_ttl_converter = converter_create("gre_ttl", converter_attribute_fallback);
	converter_loadEntry(gre_ttl_converter, 0, "inherit");

	gre_tos_converter = converter_create("gre_tos", converter_attribute_fallback);
	converter_loadEntry(gre_tos_converter, 1, "inherit");

	link_register_info(&gre_gre_info);
	link_register_info(&gre_gretap_info);
	link_register_info(&gre_ip6gre_info);
	link_register_info(&gre_ip6gretap_info);

	object_t *object = pcb_getObject(pcb, "NetDev.GRE", 0);
	object_setWriteHandler(object, gre_writeObject);
	object_setInstanceDelHandler(object, gre_delInstance);
	parameter_setValidator(object_getParameter(object, "IFlags"), param_validator_create_custom(validateConverterParameter, gre_flags_converter));
	parameter_setValidator(object_getParameter(object, "OFlags"), param_validator_create_custom(validateConverterParameter, gre_flags_converter));
	parameter_setValidator(object_getParameter(object, "Local"), param_validator_create_custom(gre_validateAddr, NULL));
	parameter_setValidator(object_getParameter(object, "Remote"), param_validator_create_custom(gre_validateAddr, NULL));
	parameter_setValidator(object_getParameter(object, "TTL"), param_validator_create_custom(validateConverterParameter, gre_ttl_converter));
	parameter_setValidator(object_getParameter(object, "TOS"), param_validator_create_custom(validateConverterParameter, gre_tos_converter));
}

void gre_cleanup() {
	link_unregister_info(&gre_gre_info);
	link_unregister_info(&gre_gretap_info);
	link_unregister_info(&gre_ip6gre_info);
	link_unregister_info(&gre_ip6gretap_info);

	converter_destroy(gre_flags_converter);
	converter_destroy(gre_ttl_converter);
	converter_destroy(gre_tos_converter);
}
