/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "util.h"

static pcb_t *pcb = NULL;

typedef struct _param {
	const char *name;
	const char *pathfmt;
	char *(*print)(const variant_t *value);
} param_t;

static char *printRtrSolicitationInterval(const variant_t *value) {
	char *retval = NULL;
	if (asprintf(&retval, "%lu", (unsigned long)variant_uint32(value) / 1000) == -1)
		return NULL;
	return retval;
}

static param_t params[] = {
	{ "IPv4Forwarding",              "/proc/sys/net/ipv4/conf/%s/forwarding",                   NULL },
	{ "IPv4ForceIGMPVersion",        "/proc/sys/net/ipv4/conf/%s/force_igmp_version",           NULL },
	{ "IPv4AcceptSourceRoute",       "/proc/sys/net/ipv4/conf/%s/accept_source_route",          NULL },
	{ "IPv4AcceptRedirects",         "/proc/sys/net/ipv4/conf/%s/accept_redirects",             NULL },
	{ "IPv4ArpFilter",               "/proc/sys/net/ipv4/conf/%s/arp_filter",                   NULL },
	{ "IPv4AcceptLocal",             "/proc/sys/net/ipv4/conf/%s/accept_local",                 NULL },
	{ "IPv6AcceptRA",                "/proc/sys/net/ipv6/conf/%s/accept_ra",                    NULL },
	{ "IPv6ActAsRouter",             "/proc/sys/net/ipv6/conf/%s/forwarding",                   NULL },
	{ "IPv6AutoConf",                "/proc/sys/net/ipv6/conf/%s/autoconf",                     NULL },
	{ "IPv6MaxRtrSolicitations",     "/proc/sys/net/ipv6/conf/%s/router_solicitations",         NULL },
	{ "IPv6RtrSolicitationInterval", "/proc/sys/net/ipv6/conf/%s/router_solicitation_interval", printRtrSolicitationInterval },
	{ "IPv6AcceptSourceRoute",       "/proc/sys/net/ipv6/conf/%s/accept_source_route",          NULL },
	{ "IPv6AcceptRedirects",         "/proc/sys/net/ipv6/conf/%s/accept_redirects",             NULL },
	{ "IPv6OptimisticDAD",           "/proc/sys/net/ipv6/conf/%s/optimistic_dad",               NULL },
	{ "IPv6Disable",                 "/proc/sys/net/ipv6/conf/%s/disable_ipv6",                 NULL },
	{ NULL, NULL, NULL }
};

typedef struct _job {
	llist_iterator_t it;
	int link;
	param_t *param;
	char *value;
} job_t;

static llist_t todo = {NULL, NULL};

static void job_destroy(job_t *job) {
	if (!job)
		return;
	free(job->value);
	llist_iterator_take(&job->it);
	free(job);
}

static void writeProcfsParameter(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	if (object_state(parameter_owner(parameter)) == object_state_created)
		return;
	job_t *job = calloc(1, sizeof(job_t));
	job->link = atoi(object_name(parameter_owner(parameter), 0));
	for (job->param = &params[0]; strcmp(job->param->name, parameter_name(parameter)); job->param++);
	if (job->param->print) {
		job->value = job->param->print(parameter_getValue(parameter));
	} else {
		job->value = variant_char(parameter_getValue(parameter));
	}
	llist_append(&todo, &job->it);
}

bool procfs_initialize(pcb_t *pcbref) {
	pcb = pcbref;

	object_t *object = pcb_getObject(pcb, "NetDev.Link", 0);
	int i;
	for (i=0; params[i].name; i++) {
		parameter_t *parameter = object_getParameter(object, params[i].name);
		parameter_setWriteHandler(parameter, writeProcfsParameter);
	}

	return true;
}

bool procfs_start() {
	return true;
}

void procfs_cleanup() {
	while (!llist_isEmpty(&todo))
		job_destroy((job_t *)llist_first(&todo));
}

void procfs_kick(int link, const char *name) {
	char path[100];
	FILE *file = NULL;
	job_t *job, *jobnext;

	for (job = (job_t *)llist_first(&todo); job; job = jobnext) {
		jobnext = (job_t *)llist_iterator_next(&job->it);
		if (job->link != link)
			continue;

		snprintf(path, 100, job->param->pathfmt, name);
		file = fopen(path, "w");
		if (!file) {
			SAH_TRACE_ERROR("Could not open %s for writing: %d (%s)", path, errno, strerror(errno));
			continue;
		}
		SAH_TRACE_INFO("echo %s > %s", job->value?job->value:"", path);
		fprintf(file, "%s", job->value?job->value:"");
		fclose(file);

		job_destroy(job);
	}
}

void procfs_flush(int link) {
	job_t *job, *jobnext;
	for (job = (job_t *)llist_first(&todo); job; job = jobnext) {
		jobnext = (job_t *)llist_iterator_next(&job->it);
		if (job->link != link)
			continue;
		job_destroy(job);
	}
}
