/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "util.h"
#include "rtnl.h"
#include "convert.h"
#include "route.h"

typedef struct _route {
	llist_iterator_t it;
	// key start
	unsigned char family;
	unsigned char dstlen;
	unsigned char table;
	unsigned char protocol;
	unsigned char scope;
	unsigned char type;
	unsigned char dst[ADDRSPACE];
	int priority;
	int oif;
	// key end
	unsigned char gateway[ADDRSPACE];
	unsigned char prefsrc[ADDRSPACE];
	unsigned int mtu;
	unsigned int advmss;
	unsigned int hoplimit;
	object_t *object;
	/* Delete routes with mercy: 
	   if the same route is re-created right after it was deleted,
	   in NetDev you won't notice. This happens on the target for the default route
	 */
	pcb_timer_t *mercytimer; 
} route_t;

struct rtmetric {
	struct rtattr hdr;
	unsigned int data;
};

static pcb_t *pcb = NULL;

static bool busy = false;

static llist_t routes = {NULL, NULL};

static converter_t *table_converter = NULL;
static converter_t *protocol_converter = NULL;
static converter_t *scope_converter = NULL;
static converter_t *type_converter = NULL;

static void route_destroy(route_t *route, bool deleteObject) {
	if (!route)
		return;
	if (route->object)
		object_setUserData(route->object, NULL);
	if (deleteObject && route->object) {
		busy = true;
		object_delete(route->object);
		object_commit(route->object);
		busy = false;
	}
	pcb_timer_destroy(route->mercytimer);
	llist_iterator_take(&route->it);
	free(route);
}

static void route_expireMercy(pcb_timer_t *timer, void *userdata) {
	(void)timer;
	route_destroy((route_t *)userdata, true);
}

static route_t *route_create() {
	route_t *route = calloc(1, sizeof(route_t));
	route->mercytimer = pcb_timer_create();
	pcb_timer_setUserData(route->mercytimer, route);
	pcb_timer_setHandler(route->mercytimer, route_expireMercy);
	llist_iterator_initialize(&route->it);
	llist_append(&routes, &route->it);
	return route;
}

static void route_copy(route_t *dst, route_t *src) {
	dst->family = src->family;
	dst->dstlen = src->dstlen;
	dst->table = src->table;
	dst->protocol = src->protocol;
	dst->scope = src->scope;
	dst->type = src->type;
	memcpy(dst->dst, src->dst, addrlen(dst->family));
	dst->priority = src->priority;
	dst->oif = src->oif;
	memcpy(dst->gateway, src->gateway, addrlen(dst->family));
	memcpy(dst->prefsrc, src->prefsrc, addrlen(dst->family));
	dst->mtu = src->mtu;
	dst->advmss = src->advmss;
	dst->hoplimit = src->hoplimit;
}

static bool route_match(route_t *r1, route_t *r2) {
	return 
		r1->family == r2->family && r1->dstlen == r2->dstlen && r1->table == r2->table && 
		r1->protocol == r2->protocol && r1->scope == r2->scope && r1->type == r2->type && 
		!memcmp(r1->dst, r2->dst, addrlen(r1->family)) && r1->priority == r2->priority && r1->oif == r2->oif;
}

static route_t *route_find(route_t *ref) {
	route_t *route;
	for (route = (route_t *)llist_first(&routes); route; route = (route_t *)llist_iterator_next(&route->it)) {
		if (route_match(route, ref))
			return route;
	}
	return NULL;
}

static void rtmetric_assign(struct rtmetric *rtmetric, unsigned short type, int data) {
	rtmetric->hdr.rta_type = type;
	rtmetric->hdr.rta_len = RTA_LENGTH(sizeof(int));
	rtmetric->data = data;
}

static void route2msg(rtnlmsg_t *msg, route_t *route, bool keyonly) {
	struct rtmsg *rt = (struct rtmsg *)rtnlmsg_hdr(msg);
	rt->rtm_family = route->family;
	rt->rtm_dst_len = route->dstlen;
	rt->rtm_table = route->table;
	rt->rtm_protocol = route->protocol;
	rt->rtm_scope = route->scope;
	rt->rtm_type = route->type;
	if (!addr_isNull(route->family, route->dst))
		rtnlmsg_addAttr(msg, RTA_DST, addrlen(route->family), route->dst);
	if (route->priority)
		rtnlmsg_addAttr(msg, RTA_PRIORITY, sizeof(int), &route->priority);
	if (route->oif)
		rtnlmsg_addAttr(msg, RTA_OIF, sizeof(unsigned int), &route->oif);
	if (keyonly)
		return;
	if (!addr_isNull(route->family, route->gateway))
		rtnlmsg_addAttr(msg, RTA_GATEWAY, addrlen(route->family), route->gateway);
	if (!addr_isNull(route->family, route->prefsrc))
		rtnlmsg_addAttr(msg, RTA_PREFSRC, addrlen(route->family), route->prefsrc);
	static struct rtmetric rtmetricbuf[3];
	struct rtmetric *rtmetricptr = rtmetricbuf;
	if (route->mtu)        rtmetric_assign(rtmetricptr++, RTAX_MTU,        route->mtu);
	if (route->advmss)     rtmetric_assign(rtmetricptr++, RTAX_ADVMSS,     route->advmss);
	if (route->hoplimit)   rtmetric_assign(rtmetricptr++, RTAX_HOPLIMIT,   route->hoplimit);
	if (rtmetricptr > rtmetricbuf)
		rtnlmsg_addAttr(msg, RTA_METRICS, (char *)rtmetricptr - (char *)rtmetricbuf, rtmetricbuf);
}

static void msg2route(route_t *route, rtnlmsg_t *msg) {
	memset(route, 0, sizeof(route_t));
	struct rtmsg *rt = (struct rtmsg *)rtnlmsg_hdr(msg);
	route->family = rt->rtm_family;
	route->dstlen = rt->rtm_dst_len;
	route->table = rt->rtm_table; // ignore the RTA_TABLE attribute and only consider rtmsg->rtm_table.
	route->protocol = rt->rtm_protocol;
	route->scope = rt->rtm_scope;
	route->type = rt->rtm_type;
	struct rtattr *rta;
	if ((rta = rtnlmsg_attr(msg, RTA_DST)))
		memcpy(route->dst, RTA_DATA(rta), addrlen(route->family));
	if ((rta = rtnlmsg_attr(msg, RTA_PRIORITY)))
		route->priority = *(int *)RTA_DATA(rta);
	if ((rta = rtnlmsg_attr(msg, RTA_OIF)))
		route->oif = *(int *)RTA_DATA(rta);
	if ((rta = rtnlmsg_attr(msg, RTA_GATEWAY)))
		memcpy(route->gateway, RTA_DATA(rta), addrlen(route->family));
	if ((rta = rtnlmsg_attr(msg, RTA_PREFSRC)))
		memcpy(route->prefsrc, RTA_DATA(rta), addrlen(route->family));
	if ((rta = rtnlmsg_attr(msg, RTA_METRICS))) {
		struct rtmetric *metric, *metric0 = (struct rtmetric *)RTA_DATA(rta);
		int nmetrics = RTA_PAYLOAD(rta) / NLMSG_ALIGN(sizeof(struct rtmetric));
		for (metric = metric0; metric - metric0 < nmetrics; metric++) {
			switch (metric->hdr.rta_type) {
				case RTAX_MTU:        route->mtu        = metric->data; break;
				case RTAX_ADVMSS:     route->advmss     = metric->data; break;
				case RTAX_HOPLIMIT:   route->hoplimit   = metric->data; break;
			}
		}
	}
}


static void handleNewRoute(rtnlmsg_t *msg) {
	route_t ws;
	msg2route(&ws, msg);
	// ignore some of the routes as hack to avoid abundance of uninteresting routing events on the target
	if (ws.protocol == RTPROT_UNSPEC || ws.table == RT_TABLE_LOCAL)
		return;
	busy = true;
	route_t *route = route_find(&ws);
	if (route) {
		pcb_timer_stop(route->mercytimer);
		route_copy(route, &ws);
	} else {
		char pathbuf[40];
		sprintf(pathbuf, "NetDev.Link.%d.%sRoute", ws.oif, family2str(ws.family));
		object_t *parent = pcb_getObject(pcb, pathbuf, 0);
		if (!parent) {
			SAH_TRACE_ERROR("NetDev.Link.%d.%sRoute not found", ws.oif, family2str(ws.family));
			goto leave;
		}
		route = route_create();
		sprintf(pathbuf, "dyn%u", ({static unsigned counter = 0; counter++;}));
		route->object = object_createInstance(parent, 0, pathbuf);
		object_setUserData(route->object, route);
	}
	route_copy(route, &ws);
	object_parameterSetUInt8Value(route->object, "DstLen", route->dstlen);
	object_parameterSetCharValue(route->object, "Table", convert_bin2str(table_converter, route->table, NULL));
	object_parameterSetCharValue(route->object, "Protocol", convert_bin2str(protocol_converter, route->protocol, NULL));
	object_parameterSetCharValue(route->object, "Scope", convert_bin2str(scope_converter, route->scope, NULL));
	object_parameterSetCharValue(route->object, "Type", convert_bin2str(type_converter, route->type, NULL));
	object_parameterSetCharValue(route->object, "Dst", addr2str(route->family, route->dst, false));
	object_parameterSetInt32Value(route->object, "Priority", route->priority);
	object_parameterSetCharValue(route->object, "Gateway", addr2str(route->family, route->gateway, true));
	object_parameterSetCharValue(route->object, "PrefSrc", addr2str(route->family, route->prefsrc, true));
	object_parameterSetUInt32Value(route->object, "MTU", route->mtu);
	object_parameterSetUInt32Value(route->object, "AdvMSS", route->advmss);
	object_parameterSetUInt32Value(route->object, "HopLimit", route->hoplimit);
	object_commit(route->object);
leave:
	busy = false;
}

static void handleDelRoute(rtnlmsg_t *msg) {
	route_t ws;
	msg2route(&ws, msg);
	route_t *route = route_find(&ws);
	if (!route)
		return;
	pcb_timer_start(route->mercytimer, 100);
}

static void printRouteMsg(FILE *f, rtnlmsg_t *msg) {
	fprintf(f, "%s{", 
		rtnlmsg_type(msg) == RTM_NEWROUTE ? "RTM_NEWROUTE" :
		rtnlmsg_type(msg) == RTM_DELROUTE ? "RTM_DELROUTE" :
		rtnlmsg_type(msg) == RTM_GETROUTE ? "RTM_GETROUTE" : "<UNKNOWN>");
	struct rtmsg *rt = (struct rtmsg *)rtnlmsg_hdr(msg);	
	fprintf(f, " family=%u(%s)", rt->rtm_family, family2str(rt->rtm_family));
	fprintf(f, " dst_len=%u src_len=%u tos=%u", rt->rtm_dst_len, rt->rtm_src_len, rt->rtm_tos);
	fprintf(f, " table=%u(%s)", rt->rtm_table, convert_bin2str(table_converter, rt->rtm_table, NULL));
	fprintf(f, " protocol=%u(%s)", rt->rtm_protocol, convert_bin2str(protocol_converter, rt->rtm_protocol, NULL));
	fprintf(f, " scope=%u(%s)", rt->rtm_scope, convert_bin2str(scope_converter, rt->rtm_scope, NULL));
	fprintf(f, " type=%u(%s)", rt->rtm_type, convert_bin2str(type_converter, rt->rtm_type, NULL));
	fprintf(f, " flags=0x%x", rt->rtm_flags);
	struct rtattr *rta;
	unsigned int rtalen = rtnlmsg_attrLen(msg);
	for (rta = rtnlmsg_attrFirst(msg); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
		switch (rta->rta_type) {
			case RTA_DST:      fprintf(f, " dst=%s",      addr2str(rt->rtm_family, (unsigned char *)RTA_DATA(rta), false)); break;
			case RTA_SRC:      fprintf(f, " src=%s",      addr2str(rt->rtm_family, (unsigned char *)RTA_DATA(rta), false)); break;
			case RTA_IIF:      fprintf(f, " iif=%d",      *(int *)RTA_DATA(rta)); break;
			case RTA_OIF:      fprintf(f, " oif=%d",      *(int *)RTA_DATA(rta)); break;
			case RTA_GATEWAY:  fprintf(f, " gateway=%s",  addr2str(rt->rtm_family, (unsigned char *)RTA_DATA(rta), false)); break;
			case RTA_PRIORITY: fprintf(f, " priority=%d", *(int *)RTA_DATA(rta)); break;
			case RTA_PREFSRC:  fprintf(f, " prefsrc=%s",  addr2str(rt->rtm_family, (unsigned char *)RTA_DATA(rta), false)); break;
			case RTA_TABLE: {
				unsigned int table = *(unsigned int *)RTA_DATA(rta);
				fprintf(f, " table=%u(%s)", table, convert_bin2str(table_converter, table, NULL));
				break;
			}
			case RTA_METRICS: { 
				fprintf(f, " metrics{");
				struct rtmetric *metric, *metric0 = (struct rtmetric *)RTA_DATA(rta);
				int nmetrics = RTA_PAYLOAD(rta) / NLMSG_ALIGN(sizeof(struct rtmetric));
				for (metric = metric0; metric - metric0 < nmetrics; metric++) {
					switch (metric->hdr.rta_type) {
						case RTAX_LOCK:       fprintf(f, " lock=0x%x",     metric->data); break;
						case RTAX_MTU:        fprintf(f, " mtu=%u",        metric->data); break;
						case RTAX_WINDOW:     fprintf(f, " window=%u",     metric->data); break;
						case RTAX_RTT:        fprintf(f, " rtt=%u",        metric->data); break;
						case RTAX_RTTVAR:     fprintf(f, " rttvar=%u",     metric->data); break;
						case RTAX_SSTHRESH:   fprintf(f, " ssthresh=%u",   metric->data); break;
						case RTAX_CWND:       fprintf(f, " cwnd=%u",       metric->data); break;
						case RTAX_ADVMSS:     fprintf(f, " advmss=%u",     metric->data); break;
						case RTAX_REORDERING: fprintf(f, " reordering=%u", metric->data); break;
						case RTAX_HOPLIMIT:   fprintf(f, " hoplimit=%u",   metric->data); break;
						case RTAX_INITCWND:   fprintf(f, " initcwnd=%u",   metric->data); break;
						case RTAX_FEATURES:   fprintf(f, " features=0x%x", metric->data); break;
						case RTAX_RTO_MIN:    fprintf(f, " rtomin=%u",     metric->data); break;
						default: 
							fprintf(f, "metric[%u]=%u", metric->hdr.rta_type, metric->data); 
							break;
					}
				}
				fprintf(f, " }");
				break;
			}
			case RTA_CACHEINFO: { 
				struct rta_cacheinfo *ci = (struct rta_cacheinfo *)RTA_DATA(rta);
				fprintf(f, " cacheinfo{ clntref=%lu lastuse=%lu expires=%lu error=%lu used=%lu id=%lu ts=%lu tsage=%lu }"
						, (unsigned long)ci->rta_clntref, (unsigned long)ci->rta_lastuse
						, (unsigned long)ci->rta_expires, (unsigned long)ci->rta_error
						, (unsigned long)ci->rta_used, (unsigned long)ci->rta_id
						, (unsigned long)ci->rta_ts, (unsigned long)ci->rta_tsage
						);
				break;
			}
			default:
				fprintf(f, " attr[%u](%u byte%s)", rta->rta_type, RTA_PAYLOAD(rta), RTA_PAYLOAD(rta)==1?"":"s"); 
				break;
		}
	}
	fprintf(f, " }");
}

static void writeRoute(object_t *object) {
	if (busy)
		return;
	route_t ws;
	memset(&ws, 0, sizeof(route_t));
	rtnlmsg_t *msg = rtnlmsg_instance();
	ws.family = object_family(object);
	ws.dstlen = object_parameterUInt8Value(object, "DstLen");
	ws.table = convert_str2bin(table_converter, object_parameterCharSource(object, "Table"), NULL);
	ws.protocol = convert_str2bin(protocol_converter, object_parameterCharSource(object, "Protocol"), NULL);
	ws.scope = convert_str2bin(scope_converter, object_parameterCharSource(object, "Scope"), NULL);
	ws.type = convert_str2bin(type_converter, object_parameterCharSource(object, "Type"), NULL);
	memcpy(ws.dst, str2addr(ws.family, object_parameterCharSource(object, "Dst"), NULL), addrlen(ws.family));
	ws.priority = object_parameterInt32Value(object, "Priority");
	if (ws.family == AF_INET6 && !ws.priority) // avoid automatic priority translation into 1024 -> feedback mismatch as priority is a key parameter
		ws.priority = 1;
	ws.oif = strtol(object_name(object_parent(object_parent(object)), 0), NULL, 0);
	if (!ws.oif)
		return;
	memcpy(ws.gateway, str2addr(ws.family, object_parameterCharSource(object, "Gateway"), NULL), addrlen(ws.family));
	memcpy(ws.prefsrc, str2addr(ws.family, object_parameterCharSource(object, "PrefSrc"), NULL), addrlen(ws.family));
	ws.mtu = object_parameterUInt32Value(object, "MTU");
	ws.advmss = object_parameterUInt32Value(object, "AdvMSS");
	ws.hoplimit = object_parameterUInt32Value(object, "HopLimit");
	route_t *route = object_getUserData(object);
	if (route) {
		rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_DELROUTE, sizeof(struct rtmsg), NLM_F_REQUEST);
		route2msg(msg, route, true);
		rtnl_send(msg);
		route_destroy(route, false);
		route = NULL;
	}
	while ((route = route_find(&ws))) {
		rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_DELROUTE, sizeof(struct rtmsg), NLM_F_REQUEST);
		route2msg(msg, route, true);
		rtnl_send(msg);
		route_destroy(route, true);
		rtnl_read(msg, true);
	}
	route = route_create();
	route_copy(route, &ws);
	route->object = object;
	object_setUserData(object, route);

	// If IPv6 is disabled, don't push through netlink
	if (route->family == AF_INET6 && object_parameterBoolValue(object_parent(object_parent(object)), "IPv6Disable")) {
		pcb_timer_start(route->mercytimer, 0);
		return;
	}

	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_NEWROUTE, sizeof(struct rtmsg), NLM_F_REQUEST | NLM_F_CREATE | NLM_F_REPLACE);
	route2msg(msg, route, false);
	rtnl_send(msg);
	pcb_timer_start(route->mercytimer, 2000);
}

static bool deleteRoute(object_t *object, object_t *instance) {
	(void)object;
	route_t *route = object_getUserData(instance);
	if (route) {
		rtnlmsg_t *msg = rtnlmsg_instance();
		rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_DELROUTE, sizeof(struct rtmsg), NLM_F_REQUEST);
		route2msg(msg, route, true);
		rtnl_send(msg);
		route_destroy(route, false);
	}
	return true;
}

static bool validateRouteAddr(parameter_t *parameter, void *validationData) {
	(void)validationData;
	bool success;
	str2addr(object_family(parameter_owner(parameter)), parameter_getModifiedCharSource(parameter), &success);
	return success;
}

static rtnlhandler_t newRouteHandler = { RTM_NEWROUTE, sizeof(struct rtmsg), handleNewRoute, printRouteMsg };
static rtnlhandler_t delRouteHandler = { RTM_DELROUTE, sizeof(struct rtmsg), handleDelRoute, printRouteMsg };
static rtnlhandler_t getRouteHandler = { RTM_GETROUTE, sizeof(struct rtmsg), NULL,           printRouteMsg };

bool route_initialize(pcb_t *pcbref) {
	pcb = pcbref;
	
	table_converter = converter_create("route_table", converter_attribute_fallback);
	converter_loadEntry(table_converter, RT_TABLE_UNSPEC, "unspec");
	converter_loadEntry(table_converter, RT_TABLE_DEFAULT, "default");
	converter_loadEntry(table_converter, RT_TABLE_MAIN, "main");
	converter_loadEntry(table_converter, RT_TABLE_LOCAL, "local");
	converter_loadTable(table_converter, pcb_getObject(pcb, "NetDev.ConversionTable.Table", 0));
	
	protocol_converter = converter_create("route_protocol", converter_attribute_fallback);
	converter_loadEntry(protocol_converter, RTPROT_UNSPEC, "unspec");
	converter_loadEntry(protocol_converter, RTPROT_REDIRECT, "redirect");
	converter_loadEntry(protocol_converter, RTPROT_KERNEL, "kernel");
	converter_loadEntry(protocol_converter, RTPROT_BOOT, "boot");
	converter_loadEntry(protocol_converter, RTPROT_STATIC, "static");
	converter_loadTable(protocol_converter, pcb_getObject(pcb, "NetDev.ConversionTable.Protocol", 0));
	
	scope_converter = converter_create("route_scope", converter_attribute_fallback);
	converter_loadEntry(scope_converter, RT_SCOPE_UNIVERSE, "global");
	converter_loadEntry(scope_converter, RT_SCOPE_SITE, "site");
	converter_loadEntry(scope_converter, RT_SCOPE_HOST, "host");
	converter_loadEntry(scope_converter, RT_SCOPE_LINK, "link");
	converter_loadEntry(scope_converter, RT_SCOPE_NOWHERE, "nowhere");
	converter_loadTable(scope_converter, pcb_getObject(pcb, "NetDev.ConversionTable.Scope", 0));
	
	type_converter = converter_create("route_type", 0);
	converter_loadEntry(type_converter, RTN_UNSPEC, "unspec");
	converter_loadEntry(type_converter, RTN_UNICAST, "unicast");
	converter_loadEntry(type_converter, RTN_LOCAL, "local");
	converter_loadEntry(type_converter, RTN_BROADCAST, "broadcast");
	converter_loadEntry(type_converter, RTN_ANYCAST, "anycast");
	converter_loadEntry(type_converter, RTN_MULTICAST, "multicast");
	converter_loadEntry(type_converter, RTN_BLACKHOLE, "blackhole");
	converter_loadEntry(type_converter, RTN_UNREACHABLE, "unreachable");
	converter_loadEntry(type_converter, RTN_PROHIBIT, "prohibit");
	converter_loadEntry(type_converter, RTN_THROW, "throw");
	converter_loadEntry(type_converter, RTN_NAT, "nat");

	const char *path[] = {"NetDev.Link.IPv4Route", "NetDev.Link.IPv6Route"};
	int i;
	for (i=0; i<2; i++) {
		object_t *object = pcb_getObject(pcb, path[i], 0);
		object_setWriteHandler(object, writeRoute);
		object_setInstanceDelHandler(object, deleteRoute);
		parameter_setValidator(object_getParameter(object, "Table"), param_validator_create_custom(validateConverterParameter, table_converter));
		parameter_setValidator(object_getParameter(object, "Protocol"), param_validator_create_custom(validateConverterParameter, protocol_converter));
		parameter_setValidator(object_getParameter(object, "Scope"), param_validator_create_custom(validateConverterParameter, scope_converter));
		parameter_setValidator(object_getParameter(object, "Type"), param_validator_create_custom(validateConverterParameter, type_converter));
		parameter_setValidator(object_getParameter(object, "Dst"), param_validator_create_custom(validateRouteAddr, NULL));
		parameter_setValidator(object_getParameter(object, "Gateway"), param_validator_create_custom(validateRouteAddr, NULL));
		parameter_setValidator(object_getParameter(object, "PrefSrc"), param_validator_create_custom(validateRouteAddr, NULL));
	}

	rtnl_registerHandler(&newRouteHandler);
	rtnl_registerHandler(&delRouteHandler);
	rtnl_registerHandler(&getRouteHandler);
	return true;
}

bool route_start() {	
	rtnlmsg_t *msg = rtnlmsg_instance();
	rtnlmsg_initialize(msg, rtnlmsg_buflen(msg), RTM_GETROUTE, sizeof(struct rtmsg), NLM_F_REQUEST | NLM_F_DUMP);
	if (!rtnl_send(msg))
		return false;
	if (!rtnl_read(msg, true))
		return false;
	return true;
}

void route_cleanup() {
	rtnl_unregisterHandler(&newRouteHandler);
	rtnl_unregisterHandler(&delRouteHandler);
	rtnl_unregisterHandler(&getRouteHandler);

	while (!llist_isEmpty(&routes))
		route_destroy((route_t *)llist_first(&routes), false);
	
	converter_destroy(table_converter);
	converter_destroy(protocol_converter);
	converter_destroy(scope_converter);
	converter_destroy(type_converter);
}

void route_deleteIPv4Routes(int link) {
	busy = true;
	route_t *route, *routenext;
	for (route = (route_t *)llist_first(&routes); route; route = routenext) {
		routenext = (route_t *)llist_iterator_next(&route->it);
		if (route->family == AF_INET && route->oif == link)
			route_destroy(route, true);
	}
	busy = false;
}

