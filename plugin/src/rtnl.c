/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>

#include "rtnl.h"

struct _rtnlmsg {
	unsigned int buflen;
	unsigned short hdrlen;
	struct nlmsghdr *nlmsg;
	char buf[0];
};

typedef struct _handler {
	llist_iterator_t it;
	rtnlhandler_t handler;
} handler_t;

static llist_t handlers = {NULL, NULL};

static union {
	char raw[8192];
	rtnlmsg_t msg;
} static_rtnlmsg_buffer = {.msg = {.buflen = 8192, .hdrlen = 0, .nlmsg = (struct nlmsghdr *)static_rtnlmsg_buffer.msg.buf}};

static int nlfd = -1;

static bool tracing_enabled = false;

#define historylen 8
#define historybufsize 1024
static union {
	char raw[historybufsize];
	rtnlmsg_t msg;
} history[historylen] = {
	{.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr *)history[0].msg.buf}},
	{.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr *)history[1].msg.buf}},
	{.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr *)history[2].msg.buf}},
	{.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr *)history[3].msg.buf}},
	{.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr *)history[4].msg.buf}},
	{.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr *)history[5].msg.buf}},
	{.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr *)history[6].msg.buf}},
	{.msg = {.buflen = historybufsize, .hdrlen = 0, .nlmsg = (struct nlmsghdr *)history[7].msg.buf}},
};
static int historycur = 0;
static int historymax = 0;

rtnlmsg_t *rtnlmsg_instance() {
	return &static_rtnlmsg_buffer.msg;
}

void rtnlmsg_initialize(rtnlmsg_t *msg, unsigned int buflen, unsigned short type, unsigned short hdrlen, unsigned short flags) {
	memset(msg, 0, buflen);
	msg->buflen = buflen;
	msg->hdrlen = hdrlen;
	msg->nlmsg = (struct nlmsghdr *)msg->buf;
	msg->nlmsg->nlmsg_len = NLMSG_LENGTH(hdrlen);
	msg->nlmsg->nlmsg_type = type;
	msg->nlmsg->nlmsg_flags = flags;
}

unsigned short rtnlmsg_type(rtnlmsg_t *msg) {
	return msg->nlmsg->nlmsg_type;
}

int rtnlmsg_buflen(rtnlmsg_t *msg) {
	return msg->buflen;
}

struct nlmsghdr *rtnlmsg_nlhdr(rtnlmsg_t *msg) {
	return msg->nlmsg;
}

unsigned short rtnlmsg_hdrlen(rtnlmsg_t *msg) {
	return msg->hdrlen;
}

void *rtnlmsg_hdr(rtnlmsg_t *msg) {
	return NLMSG_DATA(msg->nlmsg);
}

struct rtattr *rtnlmsg_attr(rtnlmsg_t *msg, unsigned short type) {
	struct rtattr *rta;
	unsigned int rtalen = NLMSG_PAYLOAD(msg->nlmsg, msg->hdrlen);
	for (rta = (struct rtattr *)(NLMSG_DATA(msg->nlmsg) + NLMSG_ALIGN(msg->hdrlen)); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen))
		if (rta->rta_type == type)
			return rta;
	return NULL;
}

struct rtattr *rtnlmsg_attrFirst(rtnlmsg_t *msg) {
	return (struct rtattr *)(NLMSG_DATA(msg->nlmsg) + NLMSG_ALIGN(msg->hdrlen));
}

unsigned int rtnlmsg_attrLen(rtnlmsg_t *msg) {
	return NLMSG_PAYLOAD(msg->nlmsg, msg->hdrlen);
}

void rtnlmsg_addAttr(rtnlmsg_t *msg, unsigned short type, unsigned short len, const void *data) {
	unsigned short rtalen = RTA_LENGTH(len);
	if (NLMSG_ALIGN(msg->nlmsg->nlmsg_len) + rtalen + ((char *)msg->nlmsg - (char *)msg) > msg->buflen) {
		SAH_TRACE_ERROR("Not enough room to add attribute %d to netlink message (%d bytes short)", type, 
				NLMSG_ALIGN(msg->nlmsg->nlmsg_len) + rtalen + ((char *)msg->nlmsg - (char *)msg) - msg->buflen);
		return;
	}
	struct rtattr *rta = (struct rtattr *)(((char *)msg->nlmsg) + NLMSG_ALIGN(msg->nlmsg->nlmsg_len));
	rta->rta_type = type;
	rta->rta_len = rtalen;
	if (len > 0)
		memcpy(RTA_DATA(rta), data, len);
	msg->nlmsg->nlmsg_len = NLMSG_ALIGN(msg->nlmsg->nlmsg_len) + rtalen;
}

static const char *rtnlmsg_print(rtnlmsg_t *msg) {

	if (!msg) 
		return "<< message lost >>";

	printf("\n\n");
	int i, j;
	unsigned char *raw = (unsigned char *)msg->nlmsg;
	int len = msg->nlmsg->nlmsg_len;
	for (i = 0; i < len ; i += 16) {
		printf("%04x: ", i);
		for (j = i; j < i + 16 && j < len; j++)
			printf("%02x ", raw[j]);
		for (; j < i + 16; j++)
			printf("   ");
		printf("| ");
		for (j = i; j < i + 16 && j < len; j++)
			printf("%c", raw[j] < 32 ? '.' : raw[j]);
		printf("\n");
	}

	static char msgbuf[1024];
	memset(msgbuf, 0, sizeof(msgbuf));
	FILE *f = fmemopen(msgbuf, 1024, "w");
	handler_t *h;
	for (h = (handler_t *)llist_first(&handlers); h; h = (handler_t *)llist_iterator_next(&h->it)) {
		if (h->handler.type == msg->nlmsg->nlmsg_type && h->handler.print) {
			msg->hdrlen = h->handler.hdrlen;
			h->handler.print(f, msg);
			msgbuf[1023] = '\0';
			break;
		}
	}
	fclose(f);
	if (!h)
		sprintf(msgbuf, "{ msgtype=%d }", msg->nlmsg->nlmsg_type);
	return msgbuf;
}

static void rtnlmsg_store(rtnlmsg_t *msg) {
	history[historycur].msg.hdrlen = msg->hdrlen;
	memcpy(history[historycur].msg.buf, msg->nlmsg, msg->nlmsg->nlmsg_len);
	historycur++;
	if (historymax < historylen)
		historymax++;
	historycur %= historylen;
}

static rtnlmsg_t *rtnlmsg_load(struct nlmsghdr *nlmsg) {
	int i;
	for (i=0; i<historylen && i<historymax; i++) {
		if (history[i].msg.nlmsg->nlmsg_seq == nlmsg->nlmsg_seq)
			return &history[i].msg;
	}
	return NULL;
}


bool rtnl_send(rtnlmsg_t *msg) {
	msg->nlmsg->nlmsg_seq = ({static int nlmsg_seq = 0; ++nlmsg_seq;});
	msg->nlmsg->nlmsg_pid = 0;
	struct sockaddr_nl addr;
	memset(&addr, 0, sizeof(struct sockaddr_nl));
	addr.nl_family = AF_NETLINK;
	struct iovec iov = {msg->nlmsg, msg->nlmsg->nlmsg_len};
	struct msghdr msghdr = {&addr, sizeof(struct sockaddr_nl), &iov, 1, NULL, 0, 0};
	if (tracing_enabled) {
		SAH_TRACE_NOTICE("Send message #%lu: %s", (unsigned long)msg->nlmsg->nlmsg_seq, rtnlmsg_print(msg));
	}
	rtnlmsg_store(msg);
	if (sendmsg(nlfd, &msghdr, 0) < 0) {
		SAH_TRACE_ERROR("sendmsg() failed: %s", strerror(errno));
		return false;
	}
	return true;
}

bool rtnl_read(rtnlmsg_t *msg, bool blocking) {
	ssize_t buflen;
	unsigned int ubuflen;
	bool done = false, multi = false;
	handler_t *h;
	do {
		buflen = recv(nlfd, msg->buf, msg->buflen - offsetof(rtnlmsg_t, buf), 0);
		if (buflen < 0) {
			if (errno == EINTR || errno == EAGAIN) {
				SAH_TRACE_NOTICE("recv() ignore netlink message %s (%d)\n", strerror(errno), errno);
				continue;
			}
			SAH_TRACE_ERROR("recv() netlink receive error %s (%d)\n", strerror(errno), errno);
			if (errno == ENOBUFS) {
				SAH_TRACE_NOTICE("recv() warning %s (%d)\n", strerror(errno), errno);
				continue;
			}
			return false;
		}
		if (buflen == 0) {
			SAH_TRACE_ERROR("EOF on netlink\n");
			return false;
		}

		ubuflen = (unsigned)buflen;
		for (msg->nlmsg = (struct nlmsghdr *)msg->buf; NLMSG_OK(msg->nlmsg, ubuflen); msg->nlmsg = NLMSG_NEXT(msg->nlmsg, ubuflen)) {
			if (msg->nlmsg->nlmsg_flags & NLM_F_MULTI)
				multi = true;
			switch (msg->nlmsg->nlmsg_type) {
				case NLMSG_ERROR: {
					struct nlmsgerr *nlerr = (struct nlmsgerr *)NLMSG_DATA(msg->nlmsg);
					rtnlmsg_t *origmsg = rtnlmsg_load(&nlerr->msg);
					if (origmsg && nlerr->error == -EBUSY) {
						rtnl_send(origmsg);
					} else {
						SAH_TRACE_ERROR("Error %d (%s) received for request #%lu : %s", -nlerr->error, strerror(-nlerr->error),
								(unsigned long)nlerr->msg.nlmsg_seq, rtnlmsg_print(origmsg));
					}
					break;
				} 
				case NLMSG_NOOP:
					break;
				case NLMSG_DONE:
					done = true;
					break;
				default:
					if (tracing_enabled) {
						SAH_TRACE_NOTICE("Received message #%lu : %s", (unsigned long)msg->nlmsg->nlmsg_seq, rtnlmsg_print(msg));
					}
					for (h = (handler_t *)llist_first(&handlers); h; h = (handler_t *)llist_iterator_next(&h->it)) {
						if (h->handler.type == msg->nlmsg->nlmsg_type) {
							msg->hdrlen = h->handler.hdrlen;
							if (h->handler.handle)
								h->handler.handle(msg);
						}
					}
			}
		}
	} while (blocking && !done && multi);
	return true;
}

int rtnl_initialize() {
	nlfd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
	if (nlfd < 0) {
		SAH_TRACE_ERROR("socket() failed");
		return -1;
	}
	struct sockaddr_nl bindaddr;
	memset(&bindaddr, 0, sizeof(struct sockaddr_nl));
	bindaddr.nl_family = AF_NETLINK;
	bindaddr.nl_pid = getpid();
	bindaddr.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV6_IFADDR | 
			RTMGRP_IPV4_ROUTE | RTMGRP_IPV6_ROUTE | RTMGRP_NEIGH;
	if (bind(nlfd, (struct sockaddr*) &bindaddr, sizeof(struct sockaddr_nl)) < 0) {
		SAH_TRACE_ERROR("bind() failed");
		return -1;
	}
	return nlfd;
}

static handler_t *handler_create(rtnlhandler_t *handler) {
	handler_t *h = calloc(1, sizeof(handler_t));
	h->handler.type = handler->type;
	h->handler.hdrlen = handler->hdrlen;
	h->handler.handle = handler->handle;
	h->handler.print = handler->print;
	llist_append(&handlers, &h->it);
	return h;
}

static void handler_destroy(handler_t *h) {
	llist_iterator_take(&h->it);
	free(h);
}

void rtnl_cleanup() {
	close(nlfd);
	while (!llist_isEmpty(&handlers))
		handler_destroy((handler_t *)llist_first(&handlers));
}

void rtnl_registerHandler(rtnlhandler_t *handler) {
	handler_create(handler);
}

void rtnl_unregisterHandler(rtnlhandler_t *handler) {
	handler_t *h;
	for (h = (handler_t *)llist_first(&handlers); h; h = (handler_t *)llist_iterator_next(&h->it)) {
		if (h->handler.type == handler->type &&
				h->handler.hdrlen == handler->hdrlen &&
				h->handler.handle == handler->handle &&
				h->handler.print == handler->print) {
			handler_destroy(h);
			break;
		}
	}
}

void rtnl_trace(bool enable) {
	tracing_enabled = enable;
}

