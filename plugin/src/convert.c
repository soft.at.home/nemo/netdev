/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "convert.h"

typedef struct _converter_entry {
	llist_iterator_t it;
	unsigned long bin;
	char *str;
} converter_entry_t;

struct _converter {
	llist_iterator_t it;
	llist_t list;
	object_t *table;
	const char *name;
	converter_attributes_t attributes;
};

static llist_t converters = {NULL, NULL};

static converter_entry_t *converter_entry_create(converter_t *converter, unsigned long bin, const char *str) {
	converter_entry_t *entry = calloc(1, sizeof(converter_entry_t));
	entry->bin = bin;
	entry->str = str?strdup(str):NULL;
	llist_append(&converter->list, &entry->it);
	return entry;
}

static void converter_entry_destroy(converter_entry_t *entry) {
	llist_iterator_take(&entry->it);
	free(entry->str);
	free(entry);
}

converter_t *converter_create(const char *name, converter_attributes_t attributes) {
	converter_t *converter = calloc(1, sizeof(converter_t));
	converter->name = name;
	converter->attributes = attributes;
	llist_append(&converters, &converter->it);
	return converter;
}

void converter_destroy(converter_t *converter) {
	while (!llist_isEmpty(&converter->list))
		converter_entry_destroy((converter_entry_t *)llist_first(&converter->list));
	llist_iterator_take(&converter->it);
	free(converter);
}

void converter_loadEntry(converter_t *converter, unsigned long bin, const char *str) {
	converter_entry_create(converter, bin, str);
}

void converter_loadFile(converter_t *converter, const char *filename) {
	static char line[512];
	char *lineptr;
	unsigned long bin;
	const char *str;
	FILE *file = fopen(filename, "r");
	if (!file) {
		SAH_TRACE_ERROR("Could not open file %s", filename);
		return;
	}
	while (fgets(line, sizeof(line), file)) {
		if (line[0] == '#') // skip comments
			continue;
		bin = strtoul(line, &lineptr, 0);
		if (lineptr == line)
			continue; // line does not start with a number
		while (*lineptr == ' ' || *lineptr == '\t')
			lineptr++; // skip whitespace
		str = lineptr;
		while (*lineptr > ' ')
			lineptr++;
		*lineptr = '\0'; // what follows is white space / comments / line ending -> ignore
		if (!*str)
			continue; // no key found
		converter_loadEntry(converter, bin, str);
	}
	fclose(file);
}

static bool addConverterEntry(object_t *table, object_t *object) {
	converter_t *converter;
	for (converter = (converter_t *)llist_first(&converters); converter; converter = (converter_t *)llist_iterator_next(&converter->it)) {
		if (converter->table == table)
			converter_entry_create(converter, strtoul(object_name(object, 0), NULL, 0), object_name(object, path_attr_key_notation));
	}
	return true;
}

static bool delConverterEntry(object_t *table, object_t *object) {
	converter_t *converter;
	converter_entry_t *entry, *entrynext;
	unsigned long bin = strtoul(object_name(object, 0), NULL, 0);
	for (converter = (converter_t *)llist_first(&converters); converter; converter = (converter_t *)llist_iterator_next(&converter->it)) {
		if (converter->table == table) {
			for (entry = (converter_entry_t *)llist_first(&converter->list); entry; entry = entrynext) {
				entrynext = (converter_entry_t *)llist_iterator_next(&entry->it);
				if (entry->bin == bin)
					converter_entry_destroy(entry);
			}
		}
	}
	return true;
}

void converter_loadTable(converter_t *converter, object_t *table) {
	object_t *object;
	object_for_each_instance(object, table)
		converter_loadEntry(converter, strtoul(object_name(object, 0), NULL, 0), object_name(object, path_attr_key_notation));
	object_setInstanceAddHandler(table, addConverterEntry);
	object_setInstanceDelHandler(table, delConverterEntry);
	converter->table = table;
}

static const char *_bin2str(converter_t *converter, unsigned long bin, bool *success) {
	converter_entry_t *entry;
	for (entry = (converter_entry_t *)llist_first(&converter->list); entry; entry = (converter_entry_t *)llist_iterator_next(&entry->it)) {
		if (entry->bin == bin)
			break;
	}
	if (entry) {
		if (success) *success = true;
		return entry->str;
	} else if (converter->attributes & converter_attribute_fallback) {
		static char fallbackbuf[20];
		if (success) *success = true;
		sprintf(fallbackbuf, "%lu", bin);
		return fallbackbuf;
	} else {
		if (success) *success = false;
		if (!(converter->attributes & converter_attribute_flags))
			SAH_TRACE_WARNING("Converter \"%s\" catched invalid binary value 0x%lx", converter->name, bin);
		return "";
	}
}

static unsigned long _str2bin(converter_t *converter, const char *str, bool *success) {
	converter_entry_t *entry;
	for (entry = (converter_entry_t *)llist_first(&converter->list); entry; entry = (converter_entry_t *)llist_iterator_next(&entry->it)) {
		if (!strcmp(entry->str?entry->str:"", str?str:""))
			break;
	}
	if (entry) {
		if (success) *success = true;
		return entry->bin;
	} else if (converter->attributes & converter_attribute_fallback) {
		char *end = NULL;
		unsigned long ret = str?strtol(str, &end, 0):0;
		if (success) *success = !end || !*end;
		if (end && *end)
			SAH_TRACE_WARNING("Converter \"%s\" catched invalid string value %s", converter->name, str?str:"(null)");
		return ret;
	} else {
		if (success) *success = false;
		SAH_TRACE_WARNING("Converter \"%s\" catched invalid string value %s", converter->name, str?str:"(null)");
		return 0;	
	}
}

static char flagstrbuf[256];

const char *convert_bin2str(converter_t *converter, unsigned long bin, bool *success) {
	if (converter->attributes & converter_attribute_flags) {
		char *flagstrptr = flagstrbuf;
		const char *str;
		int i, n=0;
		*flagstrptr = '\0';
		if (success) *success = true;
		for (i=0; i<32; i++) {
			if (bin & 1<<i) {
				str = _bin2str(converter, 1<<i, NULL);
				if (!*str)
					continue;
				flagstrptr += snprintf(flagstrptr, sizeof(flagstrbuf) - (flagstrptr - flagstrbuf), "%s%s", n++?" ":"", str);
				if (flagstrbuf + sizeof(flagstrbuf) < flagstrptr)
					break;
			}
		}
		return flagstrbuf;
	} else {
		return _bin2str(converter, bin, success);
	}
}

unsigned long convert_str2bin(converter_t *converter, const char *str, bool *success) {
	if (converter->attributes & converter_attribute_flags) {
		char *flagstrptr = flagstrbuf, *flag = flagstrbuf, flagstrtmp;
		unsigned long bin = 0;
		snprintf(flagstrbuf, sizeof(flagstrbuf), "%s", str?str:"");
		if (success) *success = true;
		do {
			if (!(*flagstrptr == '\0' || *flagstrptr == ' ' || *flagstrptr == '\t'))
				continue;
			if (flagstrptr > flag) {
				flagstrtmp = *flagstrptr;
				*flagstrptr = '\0';
				bin |= _str2bin(converter, flag, success);
				if (success && !*success)
					success = NULL;
				*flagstrptr = flagstrtmp;
			}
			flag = flagstrptr + 1;
		} while (*flagstrptr++);
		return bin;
	} else {
		return _str2bin(converter, str, success);
	}
}

