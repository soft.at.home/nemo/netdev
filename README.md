# netdev

## Summary

NetDev monitors the operating system's network devices.
It has a direct link with NeMo to report any updates.
NeMo can push updates done in its own data model through
NetDev to the kernel.

## Description

The NetDev (Network Device) monitors the operating system's
network devices.
The monitoring is performed using Linux Netlink, for kernel to
userspace communication.

NetDev maintains its own data model with all monitored interfaces
and their parameters exported. It has active Netlink queries for
any changes performed by the kernel and the result is pushed to
its own data model.

Updates to its own data model are pushed to the kernel, using the
same Netlink interface.

NetDev and NeMo are directly linked to each other. Changes to the NetDev
data model will notify NeMo to update its own data model.
Updates to the NeMo data model, for interfaces which are monitored
by NetDev, are pushed to NetDev.
